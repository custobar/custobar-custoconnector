Custobar_CustoConnector
=========


## Settings

Follow the installation guides from [README.md](../README.md)

## Initial scheduling

After installing the extension it is necessary to transfer the original store data to Custobar at least once. 

Go to `System > Configuration > CUSTOBAR > Custoconnector` to start the scheduling by clicking the button next to `Initial data synchronization`    . 

This will start the process of collecting data to the queue. 

It is important to note that the queue purging is halted during the initial scheduling and after that normal operation will be continued.  

Also the queue and initial scheduling uses Magento's cron cycles so updates will happen by every minute.

## Data mapping

Under `System > Configuration > CUSTOBAR > Custoconnector > Model classes to listen` you can control how data is mapped to Custobar or add custom mappings.

```
Mage_Catalog_Model_Product>products:
    name>title;
    sku>external_id;
    custobar_minimal_price>minimal_price;
    custobar_price>price;
```

Previous snipped tells how a model / value from Magento is mapped to Custobar. Right of the > is Custobar side and on the left is what the model or attribute name is in Magento.

### Mapping new data to Custobar

When the data is a standard Magento attribute/table column you can just add `attributeatmagento>attributeatcustobar` under the correct model line.
 
If the data comes from a complex relation (method) then you need to add an observer to feature the data as a mappable entity.

See how `custobar_minimal_price` is mapped with `public function extendProductData($observer)` on `app/code/local/Custobar/CustoConnector/Model/Observer.php`

## FAQ

### No data appears to Custobar views

1. Wrong details in settings
2. Magento's cron is not running correctly
3. Initial data scheduling is running

    Please check the status at `System > Configuration > CUSTOBAR > Custoconnector`

4. Initial data scheduling has failed
    
    Depending on the resources of the server there might be a need use a lower value on the `public static $INITIAL_PAGE_SIZE = 1000;` 
    found in `app/code/local/Custobar/CustoConnector/Helper/Data.php`. After that clear rows in `custobar_initial` table.
    
5. Allowed websites to send data from prevents sending data
