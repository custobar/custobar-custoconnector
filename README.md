Custobar_CustoConnector
=========

CustoConnector is used to send usage statistics from the Magento installation to the Custobar API

## Release Information

*CustoConnector for M1*

## System Requirements

* PHP 5.4 or higher
* Magento 1.9.x

NOTE: Module requires that the Magento [cron](http://docs.magento.com/m1/ce/user_guide/system-operations/cron.html) is correctly configured and running.

## Installation

*NOTE:* If you use modman or composer by symlinking remember to set symlinking enabled in magento

* Install with [modman](https://github.com/colinmollenhour/modman):

        modman clone https://bitbucket.org/custobar/custobar-custoconnector.git
    
* Install with [Magento Composer Installer](https://github.com/magento-hackathon/magento-composer-installer):

Add a `repositories` object to the `composer.json`
 
         "repositories": [
            {
                "type": "git",
                "url": "git@bitbucket.org:custobar/custobar-custoconnector.git"
            }
         ]


Add requirement `custobar/custobar_custoconnector`

        {
            "require": {
                "bragento/magento-composer-installer": "~1",
                "custobar/custobar_custoconnector": "dev-master"
            }
        }

* Install manually:
    * Download latest version [here](https://bitbucket.org/custobar/custobar-custoconnector/get/master.zip)
    * Unzip
    * Copy `app` directory into Magento
        

After adding the extension assets to the filesystem please

1. Log in to the admin
2. Flush caches
3. logout and re-login

### After installing 

Go to `System > Configuration > CUSTOBAR > Custoconnector` to setup the module.

Input the supplied `Client identifier` and `Api key`. 

At least one `Allowed websites to send data from` needs to be selected. 

You can also add the `Custobar tracking script` what can be generated at `https://clientidentifier.custobar.com/tracking-script/`.

Copy and paste the code *without* the following lines:

```js
// remove these
cstbrConfig.productId = 'place_product_id_here';
cstbrConfig.customerId = 'place_customer_id_here';
```
Remember to flush Magento's caches after changing the values.

## Documentation

See [documentation](docs/index.md)


## Upgrading notes

- To version 4.1.0 from 4.0.0

    Note! Check changes from version 4.0.0 first.
    
    Uses own custoconnector.log file 
        
    Featured new tracked variables for products: 
          
      custobar_child_ids>mage_child_ids
      custobar_parent_ids>mage_parent_ids
      
    Paste the following to your `Model classes to listen` (Remember to keep any site specific changes):
    
    ```
    Mage_Catalog_Model_Product>products:
      name>title;
      sku>external_id;
      custobar_minimal_price>minimal_price;
      custobar_price>price;
      type_id>mage_type;
      custobar_attribute_set_name>type;
      custobar_category>category;
      custobar_category_id>category_id;
      custobar_image>image;
      custobar_product_url>url;
      custobar_special_price>sale_price;
      description>description;
      custobar_language>language;
      custobar_store_id>store_id;
      custobar_child_ids>mage_child_ids;
      custobar_parent_ids>mage_parent_ids,
     Mage_Customer_Model_Customer>customers:
      firstname>first_name;
      lastname>last_name;
      id>external_id;
      email>email;
      custobar_telephone>phone_number;
      custobar_street>street_address;
      custobar_city>city;
      custobar_postcode>zip_code;
      custobar_country_id>country;
      custobar_created_at>date_joined;
      store_id>store_id,
     Mage_Customer_Model_Address>*Mage_Customer_Model_Customer:
      customer_id>id,
     Mage_Sales_Model_Order>sales:
      custobar_state>sale_state;
      increment_id>sale_external_id;
      customer_id>sale_customer_id;
      custobar_created_at>sale_date;
      customer_email>sale_email;
      store_id>sale_shop_id;
      custobar_discount>sale_discount;
      custobar_grand_total>sale_total;
      custobar_payment_method>sale_payment_method;
      custobar_order_items>magento__items,
     Mage_Newsletter_Model_Subscriber>events:
      subscriber_email>email;
      customer_id>customer_id;
      custobar_status>type;
      custobar_date>date;
      store_id>store_id
     ```

- To version 4.0.0 from 3.1.0
    
    New tracking script admin field added. Paste your tracking code there. If no tracking code present site usage wont be tracked.
    
    The new tracking code needs to be pasted without the opening and closing script tag and wihtout the lines containing:
    
       cstbrConfig.productId
       cstbrConfig.customerId
     
    Removed the possibility to require jquery as the new code works without it
    
    Also removed unnecessary fields relating the javascript token and banners.

- To version 3.1.0 from any previous versions

    New drop down added which you can use to is a website allowed to send data from. 
    
    No websites are allowed until you select them.
    
    Paste the following to your `Model classes to listen` (Remember to keep any site specific changes):
    
    ```
    Mage_Catalog_Model_Product>products:
      name>title;
      sku>external_id;
      custobar_minimal_price>minimal_price;
      custobar_price>price;
      type_id>mage_type;
      custobar_attribute_set_name>type;
      custobar_category>category;
      custobar_category_id>category_id;
      custobar_image>image;
      custobar_product_url>url;
      custobar_special_price>sale_price;
      description>description;
      custobar_language>language;
      custobar_store_id>store_id,
     Mage_Customer_Model_Customer>customers:
      firstname>first_name;
      lastname>last_name;
      id>external_id;
      email>email;
      custobar_telephone>phone_number;
      custobar_street>street_address;
      custobar_city>city;
      custobar_postcode>zip_code;
      custobar_country_id>country;
      custobar_created_at>date_joined;
      store_id>store_id,
     Mage_Customer_Model_Address>*Mage_Customer_Model_Customer:
      customer_id>id,
     Mage_Sales_Model_Order>sales:
      custobar_state>sale_state;
      increment_id>sale_external_id;
      customer_id>sale_customer_id;
      custobar_created_at>sale_date;
      customer_email>sale_email;
      store_id>sale_shop_id;
      custobar_discount>sale_discount;
      custobar_grand_total>sale_total;
      custobar_payment_method>sale_payment_method;
      custobar_order_items>magento__items,
     Mage_Newsletter_Model_Subscriber>events:
      subscriber_email>email;
      customer_id>customer_id;
      custobar_status>type;
      custobar_date>date;
      store_id>store_id
    ```
    
    After updating config > flush magento caches
