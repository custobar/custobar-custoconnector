<?php

/**
 * Class Custobar_CustoConnector_Helper_Data
 */
class Custobar_CustoConnector_Helper_Data extends Mage_Core_Helper_Abstract
{
    const MAX_ERROR_COUNT = 7200;
    protected $httpClient;
    /**
     * @var int
     */
    public static $INITIAL_PAGE_SIZE = 1000;
    /**
     * @var string
     */
    public static $CONFIG_ALLOWED_WEBSITES = "custobar/custobar_custoconnector/allowed_websites";
    /**
     * @var string
     */
    private static $CONFIG_TRACKED_MODELS_PATH = "custobar/custobar_custoconnector/models";
    /**
     * @var string
     */
    private static $CONFIG_API_PREFIX_PATH = "custobar/custobar_custoconnector/prefix";
    /**
     * @var string
     */
    private static $CONFIG_API_KEY_PATH = "custobar/custobar_custoconnector/apikey";
    /**
     * @var string
     */
    private static $CONFIG_MODE_PATH = "custobar/custobar_custoconnector/mode";
    /**
     * @var string
     */
    private static $CONFIG_TRACKING_SCRIPT = "custobar/custobar_custoconnector/tracking_script";
    /**
     * @var int
     */
    public static $ITEMS_PER_CRON = 500;

    /**
     * @var string
     */
    public static $INITIAL_CRON_CODE = "custoconnector_initial_schedule_population";


    public static $BACKEND_TRACKED_MODELS = [
        "Mage_Core_Model_Store" => [
            "to" => "shops",
            "fields" => [
                "id" => "external_id",
                "custobar_name" => "name"
            ]
        ]
    ];

    /**
     * @var array
     */
    private $trackedModels;

    /**
     *
     * @param int    $eventEntityId
     * @param int    $store_id
     * @param string $eventEntityType
     * @param int    $errors
     *
     * @return bool|Custobar_CustoConnector_Model_Schedule
     * @throws \Zend_Db_Statement_Exception
     */
    public function scheduleUpdate(
        $eventEntityId,
        $store_id,
        $eventEntityType,
        $errors = 0
    ) {
        $model = $this->getSchedulesModel();

        $model->setEntityId($eventEntityId);
        $model->setEntityType($eventEntityType);
        $model->setCreatedAt(Varien_Date::formatDate(true));
        $model->setProcessedAt("0000-00-00 00:00:00");
        $model->setErrors($errors);
        $model->setStoreId($store_id);

        /* prevents adding data */
        try {
            $model->save();
        } catch (Exception $e) {
            if (
                $e instanceof Zend_Db_Statement_Exception
                && $e->getCode() === 23000
            ) {
                return false;
            } else {
                // Keep throwing it.
                throw $e;
            }
        }

        if ($model) {
            return $model;
        }
        return false;
    }

    /**
     * @param Mage_Core_Model_Abstract $object
     *
     * @return bool|string
     */
    public function getEvenObjectTrackedModel($object)
    {
        try {
            $classesToTrack = $this->getConfigTrackedModels();
        } catch (Exception $e) {
            return false;
        }

        foreach (
            $classesToTrack
            as $className =>
            $data
        ) {
            if (is_a($object, $className)) {
                return $className;
            }
        }

        return false;
    }


    /**
     * @return array
     */
    public function getConfigTrackedModels()
    {
        /** @var array $trackedModels */
        $trackedModels = $this->trackedModels;

        if (count($this->trackedModels) === 0) {
            $values = $this->getCommaSeparatedConfigValue(
                self::$CONFIG_TRACKED_MODELS_PATH
            );

            foreach ($values as $value) {
                $split = $this->explodeAndTrimWith(":", $value);

                $modelMapSplit = array_map('trim', explode(">", $split[0]));

                $target = $modelMapSplit[1];
                $from = $modelMapSplit[0];
                $attributesMap = [];

                foreach (
                    $this->explodeAndTrimWith(";", $split[1]) as $attributes
                ) {
                    // reverse so that magento key is on left
                    $mapPair = array_reverse(
                        array_map('trim', explode(">", $attributes))
                    );
                    $attributesMap[(string)$mapPair[1]] = $mapPair[0];
                }

                $isInternal = false;

                if (strpos($target, "*") !== false) {
                    $isInternal = true;
                    $target = ltrim($target, "*");
                }

                $trackedModels[(string)$from]["to"] =
                    $target;
                $trackedModels[(string)$from]["internal"] =
                    $isInternal;

                $trackedModels[(string)$from]["fields"] =
                    $attributesMap;
            }

            if (count($trackedModels) == 0) {
                $this->log("No tracked models set!");
            }

            $trackedModels =
                array_merge($trackedModels, self::$BACKEND_TRACKED_MODELS);
            $this->trackedModels = $trackedModels;
        }

        return $trackedModels;
    }

    /**
     * Returns comma separated store configs as array
     *
     * @param string                                     $path
     * @param null|string|bool|int|Mage_Core_Model_Store $store
     *
     * @return array
     */
    public function getCommaSeparatedConfigValue($path, $store = null)
    {
        $configs = trim(Mage::getStoreConfig($path, $store));
        if ($configs) {
            return $this->explodeAndTrimWith(",", $configs);
        }

        return array();
    }

    /**
     * @return string
     */
    public function getApiPrefix()
    {
        return trim(Mage::getStoreConfig(self::$CONFIG_API_PREFIX_PATH, null));
    }

    /**
     * @return string
     */
    public function getApiBaseUri()
    {
        $apiPrefix = $this->getApiPrefix();

        if ($this->getIsDevModeFromConfig()) {
            $apiPrefix = "dev";
        }

        $uri = "https://{$apiPrefix}.custobar.com/api/";

        return $uri;
    }

    /**
     * @param string          $host
     * @param string|resource $rawData
     *
     * @return Varien_Http_Client
     * @throws Zend_Http_Client_Exception
     */
    public function getApiClient($host, $rawData)
    {
        $config = array(
            'timeout' => 5
        );

        $auth = $this->getApiPrefix() . "-api";
        $client = $this->getClient();
        $client->setUri($host)
            ->setConfig($config)
            ->setHeaders("accept-encoding", "application/json")
            ->setRawData($rawData, "application/json")
            ->setMethod(Zend_Http_Client::POST)
            ->setAuth($auth, $this->getApiKey());
        return $client;
    }

    /**
     * @throws \Exception
     */
    public function sendUpdateRequests()
    {
        if (!$this->canProcess()) {
            return;
        }
        /** @var Custobar_CustoConnector_Model_Mysql4_Schedule_Collection $schedules */
        $schedules = Mage::getSingleton("custoconnector/schedule")
            ->getCollection()
            ->addOnlyForSendingFilter()
            ->setOrder("created_at", "ASC")
            ->setOrder("entity_type", "ASC")
            ->setPageSize(self::$ITEMS_PER_CRON)
            ->setCurPage(1);

        /** @var array $entityLists */
        $entityLists = [];
        $types = array_unique($schedules->getColumnValues("entity_type"));

        // intialize empty holder
        foreach ($types as $type) {
            $entityLists[(string)$type] = [];
        }

        foreach ($types as $type) {
            $schedulesByType =
                $schedules->getItemsByColumnValue("entity_type", $type);

            /** @var array $itemsForProcessing */
            $itemsForProcessing = ["schedules" => [], "entities" => []];

            /** @var Custobar_CustoConnector_Model_Schedule $schedule */
            foreach ($schedulesByType as $schedule) {

                /** @var Mage_Core_Model_Abstract $entity */
                $entity = $this->getEntityFromSchedule($schedule);
                $itemsForProcessing = $this->returnItemForProcessing(
                    $entity,
                    $schedule,
                    $itemsForProcessing
                );
            }

            if (count($itemsForProcessing["entities"]) > 0) {
                $this->processCustobarResponse($itemsForProcessing);
            }
        }
    }


    /**
     * @return string
     */
    public function getApiKey()
    {
        $apiKey = trim(Mage::getStoreConfig(self::$CONFIG_API_KEY_PATH, null));

        if ($apiKey === "") {
            $this->log("Missing api key!");
            return false;
        }

        return $apiKey;
    }

    /**
     * @return bool
     */
    public function getIsDevModeFromConfig()
    {
        return (bool)trim(Mage::getStoreConfig(self::$CONFIG_MODE_PATH, null));
    }

    /**
     * @return mixed
     */
    public function getTrackingScript()
    {
        return trim(
            Mage::getStoreConfig(self::$CONFIG_TRACKING_SCRIPT, null)
        );
    }

    /**
     * @param Mage_Core_Model_Abstract $entity
     *
     * @return Varien_Object
     */
    public function getMappedEntity($entity)
    {
        $models = $this->getConfigTrackedModels();

        $className = $this->getEvenObjectTrackedModel($entity);

        /** @var array $targetModel */
        $targetModel = null;

        $to = new Varien_Object();

        try {
            $targetModel = $models[$className]["fields"];
        } catch (Exception $e) {
            $this->log(
                "tried to map entity {$className} that is not tracked"
            );
            return $to;
        }

        Mage::dispatchEvent(
            strtolower($className) . "_custo_map_before",
            [
                "entity" => $entity,
                "map" => $targetModel
            ]
        );

        Varien_Object_Mapper::accumulateByMap(
            [$entity, 'getDataUsingMethod'],
            $to,
            $targetModel
        );

        return $to;
    }

    /**
     * @param string $delimiter
     * @param string $string
     *
     * @return array
     */
    private function explodeAndTrimWith($delimiter, $string)
    {
        return array_unique(array_map('trim', explode($delimiter, $string)));
    }

    /**
     * @param array $itemsForProcessing
     *
     * @return bool
     */
    private function processCustobarResponse($itemsForProcessing)
    {
        /** @var array $schedules */
        $schedules = $itemsForProcessing["schedules"];
        /** @var array $entities */
        $entities = $itemsForProcessing["entities"];

        $custobarJson = $this->getCustobarJsonForEntity(
            $entities
        );
        /** @var Mage_Core_Model_Abstract $firstEntity */
        $firstEntity = $entities[0];

        $url = $this->getApiBaseUri() . $this->getUrlSuffix($firstEntity);

        $body = null;
        $client = null;
        $request = null;
        $failed = false;

        try {
            $client = $this->getApiClient($url, $custobarJson);
            $request = $client->request();
            $body = trim($request->getBody());
        } catch (Exception $ex) {
            $this->log(
                $ex->getMessage(),
                Zend_Log::ERR
            );
            Mage::logException($ex);

            $failed = true;
        }

        if (empty($body)) {
            $this->log(
                "body short",
                Zend_Log::ERR
            );
            $failed = true;
        }
        $json = json_decode($body, true);
        if (!(is_array($json) && isset($json["response"])
            && strtolower($json["response"]) === "ok")
        ) {
            $this->log(
                "wrong response content: {$body}. Code {$request->getStatus()}",
                Zend_Log::ERR
            );
            $failed = true;
        }
        array_walk($schedules, array($this, "markScheduleProcessed"));

        if ($failed) {
            array_walk($schedules, array($this, "rescheduleFailedSchedule"));
        }

        return true;
    }

    /**
     * @param Custobar_CustoConnector_Model_Schedule $item
     * @param                                        $key
     */
    public function markScheduleProcessed($item, $key)
    {
        $item->setProcessedAt(
            Varien_Date::formatDate(true)
        );
        $item->save();
    }

    /**
     * @param Custobar_CustoConnector_Model_Schedule $item
     * @param                                        $key
     */
    public function rescheduleFailedSchedule($item, $key)
    {
        if ($item->getErrorsCount() < self::MAX_ERROR_COUNT) {
            $this->scheduleUpdate(
                $item->getEntityId(),
                $item->getStoreId(),
                $item->getEntityType(),
                $item->getErrorsCount() + 1
            );
        } else {
            $this->log(
                "Did not reschedule {$item->getEntityId()} of {$item->getEntityType()} due to error limits"
            );
        }
    }

    /**
     * @param Custobar_CustoConnector_Model_Schedule $schedule
     *
     * @return bool|\Mage_Core_Model_Abstract
     */
    public function getEntityFromSchedule($schedule)
    {
        if ($this->getIsModelInternal($schedule->getEntityType())) {
            return false;
        }

        return $this->getEntityByTypeAndId(
            $schedule->getEntityType(),
            $schedule->getEntityId(),
            $schedule->getStoreId()
        );
    }

    /**
     * @param Mage_Core_Model_Abstract|array $entities
     *
     * @return string
     */
    public function getCustobarJsonForEntity($entities)
    {
        if (!is_array($entities)) {
            $entities = [$entities];
        }

        $models = $this->getConfigTrackedModels();
        $keyName = null;
        $dataJson = null;

        /** @var array $entityRows */
        $entityRows = [];

        /** @var Mage_Core_Model_Abstract $entity */
        foreach ($entities as $entity) {
            $mapClass = $this->getEvenObjectTrackedModel($entity);

            /** @var array $entityData */
            $entityData = $this->getMappedEntity($entity)->getData();

            /** @var string $keyName */
            $keyName = $models[$mapClass]["to"];

            if ($mapClass === "Mage_Sales_Model_Order") {
                /** @var array $itemData */
                $itemData = $entityData["magento__items"];
                unset($entityData["magento__items"]);

                if (count($itemData) > 0) {

                    // merge the first product to the first set
                    $first = array_shift($itemData);
                    $entityData = array_merge($entityData, $first);
                }
                $entityRows =
                    array_merge($entityRows, [$entityData], $itemData);
            } else {
                $entityRows[] = $entityData;
            }
        }
        $dataJson = json_encode([$keyName => $entityRows]);

        return $dataJson;
    }

    /**
     * @param Mage_Core_Model_Abstract $entity
     *
     * @return string
     */
    public function getUrlSuffix($entity)
    {
        $models = $this->getConfigTrackedModels();
        $mapClass = $this->getEvenObjectTrackedModel($entity);

        $keyName = $models[$mapClass]["to"];
        return "{$keyName}/upload/";
    }

    /**
     * @param string  $message
     * @param integer $level
     */
    public function log($message, $level = null)
    {
        Mage::log(
            "CustoConnector: {$message}",
            $level,
            "custoconnector.log"
        );
    }

    public function addInitialCronSchedules()
    {
        $job_code = Custobar_CustoConnector_Helper_Data::$INITIAL_CRON_CODE;
        $timestamp = strftime("%Y-%m-%d %H:%M:%S");


        $schedule = $this->getCronModel();
        $schedule->setJobCode($job_code)
            ->setCreatedAt($timestamp)
            ->setScheduledAt($timestamp)
            ->setStatus(Mage_Cron_Model_Schedule::STATUS_PENDING)
            ->save();
    }

    /**
     * @param string $whatModel
     */
    public function startInitialScheduling($whatModel = null)
    {
        if (!$this->canProcess()) {
            return;
        }
        /** @var Mage_Core_Model_Resource_Store_Collection $stores */
        $trackedModels = array_keys($this->getConfigTrackedModels());
        /** @var int $succesfull */
        $successful = 0;
        foreach ($trackedModels as $model) {
            if ($whatModel && ($model !== $whatModel)) {
                continue;
            }

            /** skip internally tracked models as they dont go to the schedules table  */
            if ($this->getIsModelInternal($model)) {
                continue;
            }

            /** @var Mage_Core_Model_Abstract $modelSingleton */
            $modelSingleton = Mage::getSingleton($model);
            if ($modelSingleton) {
                /** @var Mage_Core_Model_Mysql4_Collection_Abstract $collection */
                $collection = $modelSingleton->getCollection();

                if ($collection->getSize() > 0) {
                    try {
                        $collection->setPageSize(self::$INITIAL_PAGE_SIZE);

                        /** @var Custobar_CustoConnector_Model_Initial $initial */
                        $initial =
                            Mage::getModel("custoconnector/initial");

                        $initial->setPage(0);
                        $initial->setPages($collection->getLastPageNumber());
                        $initial->setEntityType($model);
                        $initial->save();
                        $successful++;
                    } catch (Exception $e) {
                        // dont handle exceptions as we prevent adding same again
                    }
                }
            }
        }
        if ($successful > 0) {
            $this->addInitialCronSchedules();
        }
    }

    public function handleNextInitialPage()
    {
        /** @var Custobar_CustoConnector_Model_Initial $initialModel */
        $initialModel = $this->getInitialModel();
        /** @var Custobar_CustoConnector_Model_Mysql4_Initial_Collection $initialSchedulesCollection */
        $initialSchedulesCollection = $initialModel->getCollection();
        /** @var Custobar_CustoConnector_Model_Initial $initialItem */

        $skipped = 0;

        foreach ($initialSchedulesCollection as $initialItem) {

            // lets skip if whe have done that set
            if ($initialItem->getPage() == $initialItem->getPages()) {
                $skipped++;
                continue;
            }
            if (!$initialItem->isEmpty()) {
                $entityType = $initialItem->getEntityType();

                /** @var Mage_Core_Model_Abstract $modelSingleton */
                $modelSingleton =
                    Mage::getModel($entityType);
                if ($modelSingleton) {
                    $initialItem->setPage((int)$initialItem->getPage() + 1);

                    $entityCollection =
                        $modelSingleton->getResourceCollection();
                    $entityCollection->setPageSize(self::$INITIAL_PAGE_SIZE);
                    $entityCollection->setCurPage($initialItem->getPage());

                    if (is_a($modelSingleton, "Mage_Catalog_Model_Product")) {
                        /** @var Mage_Catalog_Model_Resource_Product_Collection $productCollection */
                        $productCollection = $entityCollection;
                        $productCollection->addWebsiteNamesToResult();

                        /** @var Mage_Catalog_Model_Product $entity */
                        foreach ($entityCollection as $entity) {
                            $storeIds = $this->getStoreIdsByWebsites(
                                $entity->getWebsites()
                            );

                            foreach ($storeIds as $storeId) {
                                $this->scheduleUpdate(
                                    $entity->getId(),
                                    $storeId,
                                    $initialItem->getEntityType()
                                );
                            }
                        }
                    } else {
                        foreach ($entityCollection as $entity) {
                            $this->scheduleUpdate(
                                $entity->getId(),
                                $entity->getStoreId(),
                                $initialItem->getEntityType()
                            );
                        }
                    }


                    $initialItem->save();
                } else {
                    /** remove an unknown model schedule */
                    $initialItem->delete();
                }
                $this->addInitialCronSchedules();
                /** break the for loop so we don't handle next set */
                break;
            }
        }
        // If we have skipped all done sets lest clear the collection
        if ($skipped >= $initialSchedulesCollection->getSize()) {
            $initialModel->cleanInitials();
        }
    }

    /**
     * @param string $job_code
     *
     * @return \Mage_Cron_Model_Resource_Schedule_Collection
     */
    public function getCronSchedulesByCode($job_code)
    {
        $schedules = $this->getCronCollection();
        $schedules->addFilter('job_code', $job_code);
        return $schedules;
    }

    /**
     * @return Mage_Cron_Model_Schedule
     */
    private function getCronModel()
    {
        return Mage::getModel('cron/schedule');
    }

    /**
     * @return Mage_Cron_Model_Resource_Schedule_Collection
     */
    private function getCronCollection()
    {
        return $this->getCronModel()->getCollection();
    }

    public function canProcess()
    {
        return ($this->getApiKey() && $this->getApiPrefix());
    }

    public function getIsModelInternal($className)
    {
        $set = $this->getModelMapSet($className);

        if (isset($set["internal"]) && $set["internal"] == true) {
            return true;
        }

        return false;
    }

    /**
     * @param string $className
     *
     * @return bool|mixed
     */
    public function getModelMapSet($className)
    {
        $models = $this->getConfigTrackedModels();

        if (isset($models[$className])) {
            return $models[$className];
        }
        return false;
    }

    /**
     * @return \Custobar_CustoConnector_Model_Mysql4_Schedule_Collection
     */
    public function getSchedulesCollection()
    {
        /** @var Custobar_CustoConnector_Model_Mysql4_Schedule_Collection $collection */
        $collection = $this->getSchedulesModel()->getCollection();
        return $collection;
    }

    /**
     * @return \Custobar_CustoConnector_Model_Schedule
     */
    public function getSchedulesModel()
    {
        /** @var Custobar_CustoConnector_Model_Schedule $model */
        $model = Mage::getModel("custoconnector/schedule");
        return $model;
    }

    /**
     * @param $websiteId
     *
     * @return bool
     */
    public function getIsWebsiteAllowed($websiteId)
    {
        $values = $this->getCommaSeparatedConfigValue(
            self::$CONFIG_ALLOWED_WEBSITES
        );

        $values[] = 0; // add the admin store id

        if (count($values) > 0 && in_array($websiteId, $values)) {
            return true;
        }

        return false;
    }

    /**
     * @param Mage_Core_Model_Abstract $ob
     *
     * @return bool
     */
    public function getIsInAllowedWebsites($ob)
    {
        if ($ob->hasData("website_id")) {
            return $this->getIsWebsiteAllowed((int)$ob->getData("website_id"));
        }

        if (method_exists($ob, "getWebsiteId")) {
            return $this->getIsWebsiteAllowed($ob->getWebsiteId());
        }

        if (method_exists($ob, "getWebsiteIds")) {
            $webSiteIds = $ob->getWebsiteIds();
            foreach ($webSiteIds as $id) {
                if ($this->getIsWebsiteAllowed($id)) {
                    return true;
                }
            }

            return false; // need to break out here the store id we might get is 0 on products in the next
        }

        if ($ob->getStoreId() !== null) {
            if ($this->getIsWebsiteAllowed(
                Mage::getModel('core/store')
                    ->load($ob->getStoreId())
                    ->getWebsiteId()
            )
            ) {
                return true;
            }
        }
        return false;
    }

    public function getCanTrackObject($ob)
    {
        if ($ob && !$ob->isEmpty()) {
            $can = $this->getIsInAllowedWebsites($ob);
            // TODO: add a dispatch here so we have a nice hook point to do other kinds of rejections
            return $can;
        } else {
            return false;
        }
    }

    /**
     * @param string $entityType
     * @param int    $entityId
     * @param int    $storeId
     *
     * @return bool|\Mage_Core_Model_Abstract
     */
    public function getEntityByTypeAndId($entityType, $entityId, $storeId)
    {
        /** @var Mage_Core_Model_Abstract $entity */
        $entity = Mage::getModel($entityType);

        if (!$this->getEvenObjectTrackedModel($entity)) {
            return false;
        }
        if ($entity) {
            if (is_a($entity, "Mage_Catalog_Model_Product")) {
                $entity = $this->getProductEntity($entityId, $storeId);
            } else {
                $entity->load(
                    $entityId
                );
            }
        }
        return $entity;
    }

    /**
     * @param Mage_Core_Model_Abstract               $entity
     * @param Custobar_CustoConnector_Model_Schedule $schedule
     * @param array                                  $itemsForProcessing
     *
     * @return array
     */
    public function returnItemForProcessing(
        $entity,
        $schedule,
        &$itemsForProcessing
    ) {
        /** skip internally tracked models as they dont go to the schedules table  */

        if ($entity && !$entity->isEmpty()) {
            if ($this->getIsInAllowedWebsites(
                $entity
            )) {
                $itemsForProcessing["schedules"][] = $schedule;
                $itemsForProcessing["entities"][] = $entity;
            } else {
                // mark items not tracked anymore processed
                $schedule->setProcessedAt(
                    Varien_Date::formatDate(true)
                );
                $schedule->save();
                $this->log(
                    "Did not process {$schedule->getEntityId()} of {$schedule->getEntityType()}. Not in allowed websites."
                );
            }
        } else {

            if ($entity === false) {
                // entity was false
                $schedule->setProcessedAt(
                    Varien_Date::formatDate(true)
                );
                $schedule->save();

                $this->log(
                    "Did not process {$schedule->getEntityId()} of {$schedule->getEntityType()}. Model not present."
                );
            } else {
                // entity not present in the system for some reason. Maybe DB out of sync.
                $schedule->setProcessedAt(
                    Varien_Date::formatDate(true)
                );
                $schedule->save();

                $this->log(
                    "Could not find {$schedule->getEntityId()} of {$schedule->getEntityType()}. Deleted or data missing."
                );
            }
        }
        return $itemsForProcessing;
    }

    /**
     * @return \Varien_Http_Client
     */
    public function getClient()
    {
        if (!$this->httpClient) {
            $this->setClient();
        }
        return $this->httpClient;
    }

    public function setClient($client = null)
    {
        $this->httpClient = ($client) ? $client : new Varien_Http_Client();
    }

    private function getStoreIdsByWebsites($websites)
    {
        /** @var Mage_Core_Model_Mysql4_Store_Collection $storesCollection */
        $storesCollection = Mage::getModel("core/store")->getCollection();
        return $storesCollection->addWebsiteFilter($websites)->getAllIds();
    }

    public function isInitialRunning()
    {
        /** @var Custobar_CustoConnector_Model_Mysql4_Initial_Collection $rC */
        return ($this->getInitialModel()->getCollection()->getSize() > 0);
    }

    /**
     * @return false|\Custobar_CustoConnector_Model_Initial
     */
    public function getInitialModel()
    {
        return Mage::getModel("custoconnector/initial");
    }

    private function getProductEntity($entityId, $storeId)
    {
        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection = Mage::getModel("catalog/product")
            ->setStoreId($storeId)
            ->getCollection();

        /** @var Mage_Catalog_Model_Config $config */
        $config = Mage::getSingleton("catalog/config");
        $productAttributes =
            $config->getProductAttributes();

        $productAttributes[] = "description";
        $productAttributes[] = "image";
        $productAttributes[] = "visibility";

        $collection->setStoreId($storeId);
        $collection
            ->addAttributeToSelect($productAttributes)
            ->addAttributeToFilter("entity_id", $entityId);

        $collection->addPriceData();

        // change the price inner join to a left join so we get products that are out of stock
        $select = $collection->getSelect();
        $fromAndJoins = $select->getPart(Zend_Db_Select::FROM);

        foreach ($fromAndJoins as $item => $value) {
            if ($item === "price_index") {
                $value["joinType"] = "left join";
                $fromAndJoins[$item] = $value;
            }
        }

        $select->setPart(Zend_Db_Select::FROM, $fromAndJoins);

        // these iterate loaded items
        $collection->addWebsiteNamesToResult();
        $collection->addCategoryIds();

        /** @var Mage_Catalog_Model_Product $firstItem */
        $firstItem = $collection->getFirstItem();

        if (!$firstItem->isEmpty()) {
            $firstItem->setStoreId($storeId);
        }

        return $firstItem;
    }

    public function unScheduleUpdate($entityId, $modelClassName)
    {
        /** @var Custobar_CustoConnector_Model_Mysql4_Schedule_Collection $schedules */
        $schedules = Mage::getSingleton("custoconnector/schedule")
            ->getCollection()
            ->addOnlyForSendingFilter()
            ->setOrder("created_at", "ASC")
            ->setOrder("entity_type", "ASC");

        $schedules->addFieldToFilter("entity_id", $entityId);
        $schedules->addFieldToFilter("entity_type", $modelClassName);

        /** @var \Custobar_CustoConnector_Model_Schedule $schedule */
        foreach ($schedules as $schedule) {
            $schedule->setProcessedAt(
                Varien_Date::formatDate(true)
            );
            $schedule->save();
        }
    }
}
