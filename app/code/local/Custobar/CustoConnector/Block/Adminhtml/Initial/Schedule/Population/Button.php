<?php

class Custobar_CustoConnector_Block_Adminhtml_Initial_Schedule_Population_Button
    extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate(
            'custoconnector/initial/schedule/population/button.phtml'
        );
    }

    /**
     * Return element html
     *
     * @param  Varien_Data_Form_Element_Abstract $element
     *
     * @return string
     */
    protected function _getElementHtml(
        Varien_Data_Form_Element_Abstract $element
    ) {
        return $this->_toHtml();
    }

    /**
     * Generate button html
     *
     * @return string
     */
    public function getButtonHtml()
    {
        $button = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setData(
                array(
                    'id' => 'custoconnector_initial_schedule_population',
                    'label' => $this->helper('custoconnector')->__('Start'),
                    'onclick' => 'javascript:custoconnector_start_initial_schedule_population(); return false;'
                )
            );
        return $button->toHtml();
    }
}
