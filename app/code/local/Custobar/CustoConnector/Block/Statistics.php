<?php

class Custobar_CustoConnector_Block_Statistics extends Mage_Core_Block_Template
{
    /**
     * @var Custobar_CustoConnector_Helper_Data
     */
    private $helper;

    /**
     * @return Mage_Customer_Model_Customer|null
     */
    public function getCustomer()
    {
        /** @var Mage_Customer_Model_Session $session */
        $session = Mage::getSingleton('customer/session');

        if ($session->isLoggedIn()) {
            return $session->getCustomer();
        }

        return null;
    }

    /**
     * @return Mage_Catalog_Model_Product
     */
    public function getProduct()
    {
        /** @var Mage_Catalog_Model_Product $product */
        $product = Mage::registry('current_product');
        return $product;
    }

    /**
     * @return Custobar_CustoConnector_Helper_Data
     */
    public function getCHelper()
    {
        /** @var Custobar_CustoConnector_Helper_Data $helper */
        $helper = Mage::helper("custoconnector");
        if (!$helper) {
            $this->helper = Mage::helper("custoconnector");
        }
        return $helper;
    }

    /**
     * @return string
     */
    public function getTrackingScript()
    {
        return $this->getCHelper()->getTrackingScript();
    }

    public function isProductView()
    {
        if (in_array(
            "catalog_product_view",
            $this->getLayout()->getUpdate()->getHandles()
        )) {
            return true;
        }
        return false;
    }

    public function isAllowedWebsite()
    {
        $store = Mage::app()->getStore();
        $websiteId = $store->getWebsiteId();
        return $this->getCHelper()->getIsWebsiteAllowed($websiteId);
    }

    public function canTrack()
    {
        if (
            $this->getCHelper()->getTrackingScript()
            && $this->isAllowedWebsite()
        ) {
            return true;
        }
        return false;
    }
}
