<?php
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();
$sql
    = <<<SQLTEXT
# nothing here
SQLTEXT;

$installer->run($sql);
$scheduleTableName = $installer->getTable('custoconnector/schedule');

$scheduleTable = $installer->getConnection()
    ->newTable($scheduleTableName)
    ->addColumn(
        'id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true
        ),
        'inventory ID'
    )
    ->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        array(),
        'Id of the entity stored'
    )
    ->addColumn(
        'entity_type',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        255,
        array(),
        'Type of the entity stored'
    )
    ->addColumn(
        'created_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array('nullable' => false),
        'Created Time'
    )
    ->addColumn(
        'processed_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array('nullable' => false, 'default' => "0000-00-00 00:00:00"),
        'Finish Time'
    )
    ->addIndex(
        $installer->getIdxName(
            'custoconnector/schedule',
            array('entity_type', 'entity_id', 'processed_at'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('entity_type', 'entity_id', 'processed_at'),
        array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE)
    );
$installer->getConnection()->createTable($scheduleTable);

$installer->endSetup();
