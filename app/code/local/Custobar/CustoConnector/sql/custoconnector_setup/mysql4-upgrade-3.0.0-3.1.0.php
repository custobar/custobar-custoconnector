<?php
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();
$sql
    = <<<SQLTEXT
# nothing here
SQLTEXT;

$installer->run($sql);

$scheduleTableName = $installer->getTable('custoconnector/schedule');
$installer->getConnection()->addColumn(
    $scheduleTableName,
    'store_id',
    [
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable' => false,
        'default' => 0,
        'after' => null,
        'comment' => 'Store view id when the event happened'
    ]

);
$installer->getConnection()->dropIndex(
    $scheduleTableName,
    $installer->getIdxName(
        'custoconnector/schedule',
        array('entity_type', 'entity_id', 'processed_at'),
        Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
    )
);
$installer->getConnection()->addIndex(
    $scheduleTableName,
    $installer->getIdxName(
        'custoconnector/schedule',
        array('entity_type', 'entity_id', 'store_id', 'processed_at'),
        Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
    ),
    array('entity_type', 'entity_id', 'store_id', 'processed_at'),
    Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
);

$installer->endSetup();
