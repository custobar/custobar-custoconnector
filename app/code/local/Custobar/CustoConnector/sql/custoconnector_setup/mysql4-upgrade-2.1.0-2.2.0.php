<?php
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();
$sql
    = <<<SQLTEXT
# nothing here
SQLTEXT;

$installer->run($sql);

$scheduleTableName = $installer->getTable('custoconnector/schedule');
$installer->getConnection()->addColumn(
    $scheduleTableName,
    'errors',
    [
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable' => false,
        'default' => 0,
        'after' => null,
        'comment' => 'Cumulative error count'
    ]
);

$installer->endSetup();
