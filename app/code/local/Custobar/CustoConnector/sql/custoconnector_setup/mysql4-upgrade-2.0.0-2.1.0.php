<?php
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();
$sql
    = <<<SQLTEXT
# nothing here
SQLTEXT;

$installer->run($sql);
$scheduleTableName = $installer->getTable('custoconnector/initial');

$scheduleTable = $installer->getConnection()
    ->newTable($scheduleTableName)
    ->addColumn(
        'id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true
        ),
        'initial id'
    )
    ->addColumn(
        'page',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(),
        'current page'
    )
    ->addColumn(
        'pages',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(),
        'total pages'
    )
    ->addColumn(
        'entity_type',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        255,
        array(),
        'Type of the entity stored'
    )
    ->addColumn(
        'created_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array('nullable' => false),
        'Created Time'
    )
    ->addIndex(
        $installer->getIdxName(
            'custoconnector/initial',
            array('entity_type'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('entity_type'),
        array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE)
    );
$installer->getConnection()->createTable($scheduleTable);

$installer->endSetup();
