<?php

/**
 * Class Custobar_CustoConnector_Test_Block_Statistics
 *
 * @group Custobar_Block
 * @group Custobar_Statistics
 */
class Custobar_CustoConnector_Test_Block_Statistics
    extends EcomDev_PHPUnit_Test_Case_Controller
{

    /**
     * @var Custobar_CustoConnector_Helper_Data
     */
    private $helper;

    protected function setUp()
    {
        parent::setUp();
        static $registry;

        if (!$registry) {
            $registry
                = EcomDev_Utils_Reflection::getRestrictedPropertyValue(
                'Mage',
                '_registry'
            );
        } else {
            EcomDev_Utils_Reflection::setRestrictedPropertyValue(
                'Mage',
                '_registry',
                $registry
            );
        }

        $this->helper = Mage::helper("custoconnector");
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     * @loadFixture statistics.yaml
     */
    public function testNotLoggedIn()
    {
        $actualTrackingScript = $this->helper->getTrackingScript();

        $this->guestSession();
        $store = $this->app()->getStore(21);
        $this->setCurrentStore($store);

        Mage::app()->getStore()
            ->setConfig(
                "custobar/custobar_custoconnector/tracking_script",
                ""
            );

        $this->dispatch(
            "",
            array(
                '_store' => "books_eng"
            )
        );

        $this->assertResponseBodyNotContains(
            "'https://www.custobar.com/js/v1/custobar.js', 'cstbr', 'cstbrConfig'",
            "Assert that code is not present as its set to empty"
        );


        Mage::app()->getStore()
            ->setConfig(
                "custobar/custobar_custoconnector/tracking_script",
                $actualTrackingScript
            );

        $this->dispatch(
            "",
            array(
                '_store' => "books_eng"
            )
        );

        $this->assertResponseBodyContains(
            "'https://www.custobar.com/js/v1/custobar.js', 'cstbr', 'cstbrConfig'",
            "Assert that we get the code when its set"
        );

        $this->assertResponseBodyNotContains(
            "cstbrConfig.customerId = \"1\";",
            "assert that no customer details is present"
        );
        $this->assertResponseBodyNotContains(
            "cstbrConfig.productId = 'BOOK';",
            "assert that product line is not outputted"
        );

        $requestRoute = 'catalog/product/view';
        $this->dispatch(
            $requestRoute,
            array(
                '_store' => "books_eng",
                'id' => 22201,
                'category' => 222
            )
        );
        $this->assertResponseBodyContains(
            "cstbrConfig.productId = \"wells-0002\";",
            "assert that we get the product sku correctly"
        );
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     * @loadFixture statistics.yaml
     */
    public function testLoggedIn()
    {
        $store = $this->app()->getStore(22);
        $this->setCurrentStore($store);
        $this->customerSession(1);

        $requestRoute = 'catalog/product/view';
        $this->dispatch(
            $requestRoute,
            array(
                '_store' => "books_eng",
                'id' => 22201,
                'category' => 222
            )
        );
        $this->assertResponseBodyContains(
            "cstbrConfig.customerId = \"1\";",
            "Is customer id present"
        );
        $this->assertResponseBodyContains(
            "cstbrConfig.productId = \"wells-0002\";",
            "Is cb.track_browse_product present"
        );
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     * @loadFixture statistics.yaml
     * @loadFixture website.yaml
     */
    public function testWebsiteNotAllowed()
    {
        $store = $this->app()->getStore(22);
        $this->setCurrentStore($store);

        $requestRoute = 'catalog/product/view';
        $this->dispatch(
            $requestRoute,
            array(
                '_store' => "books_eng",
                'id' => 22201,
                'category' => 222
            )
        );
        $this->assertResponseBodyNotContains(
            "'https://www.custobar.com/js/v1/custobar.js', 'cstbr', 'cstbrConfig'",
            "Assert that custobar code is not present"
        );
    }

    protected function tearDown()
    {
        parent::tearDown();
    }
}
