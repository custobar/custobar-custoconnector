<?php

/**
 * Class Custobar_CustoConnector_Test_Helper_Data
 *
 * @group Custobar_Helper
 */
class Custobar_CustoConnector_Test_Helper_Data extends EcomDev_PHPUnit_Test_Case
{
    protected $realHttpClient;

    /**
     * @var Custobar_CustoConnector_Helper_Data
     */
    private $helper;

    private $session = true;

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    public function mockHttpClientReturnOk()
    {
        $responseMock = $this->getMockBuilder("Zend_Http_Response")
            ->setMethods(array('getBody'))
            ->disableOriginalConstructor()
            ->getMock();

        $responseMock->expects($this->any())
            ->method('getBody')
            ->will(
                $this->returnValue(
                    "{\"response\": \"ok\", \"id\": \"201611111148539022881736\"}"
                )
            );

        $clientMock = $this->getMockBuilder('Varien_Http_Client')
            ->setMethods(array('request'))
            ->getMock();

        $clientMock->expects($this->any())
            ->method('request')
            ->will($this->returnValue($responseMock));
        return $clientMock;
    }

    public function clearCustobarLog()
    {
        $logFile = $this->custobarLogFile();

        if (file_exists($logFile)) {
            //create a file handler by opening the file
            $myTextFileHandler = @fopen($logFile, "w");
            @ftruncate($myTextFileHandler, 0);
            fclose($myTextFileHandler);
        }
    }

    protected function setUp()
    {
        parent::setUp();
        @session_start();
        /** @var Custobar_CustoConnector_Helper_Data $helper */

        $this->helper = Mage::helper("custoconnector");

        $this->helper->setClient(new Varien_Http_Client());
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     */
    public function testScheduleFilters()
    {
        /** @var Custobar_CustoConnector_Model_Mysql4_Schedule_Collection $schedules */
        $schedules = Mage::getSingleton("custoconnector/schedule")
            ->getCollection()
            ->setOrder("entity_id", "ASC")
            ->addOnlyForSendingFilter();

        $this->assertEquals(
            "Custobar_CustoConnector_Model_Mysql4_Schedule_Collection",
            get_class($schedules)
        );

        /** @var Custobar_CustoConnector_Model_Schedule $first */
        $first = $schedules->getFirstItem();

        $this->assertEquals(1, $first->getEntityId(), "is id right");
        $this->assertEquals(
            "Mage_Customer_Model_Customer",
            $first->getEntityType(),
            "is type right"
        );

        $this->assertEquals(
            31,
            $first->getStoreId(),
            "is the store right"
        );
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     * @loadFixture orders
     */
    public function testAddSchedules()
    {
        $this->assertNotNull($this->helper);

        $this->assertFalse(
            $this->helper->scheduleUpdate(
                1,
                31,
                "Mage_Customer_Model_Customer"
            ),
            "Testing that cant add the same again that has been added from the fixture already"
        );

        $productSchedule = $this->helper->scheduleUpdate(
            1,
            21,
            "Mage_Catalog_Model_Product"
        );

        $this->assertFalse(
            $productSchedule->isEmpty(),
            "Testing did add a product schedule"
        );

        /** @var Custobar_CustoConnector_Model_Mysql4_Schedule_Collection $schedules */
        $schedules = Mage::getSingleton("custoconnector/schedule")
            ->getCollection()
            ->addOnlyForSendingFilter();

        $data = $schedules->getData();

        $this->assertEquals(1, $data[0]["entity_id"]);
        $this->assertEquals(
            "Mage_Customer_Model_Customer",
            $data[0]["entity_type"],
            "Test that customer model is the first in the schedules"
        );

        $this->assertEquals(
            21,
            $data[3]["store_id"],
            "Test that there is a store id present in the data"
        );

        // stored state count
        $this->assertEquals(
            4,
            $schedules->count(),
            "Test that there are only 4 scheduled items"
        );
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     */
    public function testApiClient()
    {

        /** @var Custobar_CustoConnector_Model_Mysql4_Schedule_Collection $schedules */
        $schedules = Mage::getSingleton("custoconnector/schedule")
            ->getCollection()
            ->setOrder("entity_id", "ASC")
            ->addOnlyForSendingFilter();

        /** @var Custobar_CustoConnector_Model_Schedule $schedule */
        $schedule = $schedules->getFirstItem();
        $entity = $this->helper->getEntityFromSchedule($schedule);
        $this->assertEquals(
            "Mage_Customer_Model_Customer",
            get_class($entity),
            "test that the entity is Mage_Customer_Model_Customer"
        );

        $custobarJson1 = $this->helper->getCustobarJsonForEntity(
            $entity
        );

        $custobarJson2 = $this->helper->getCustobarJsonForEntity(
            [$entity]
        );

        $this->assertEquals($custobarJson1, $custobarJson2);

        Mage::app()->getStore()
            ->setConfig("custobar/custobar_custoconnector/mode", 0);

        $url = $this->helper->getApiBaseUri() . $this->helper->getUrlSuffix(
                $entity
            );

        $this->assertEquals(
            "https://dev.custobar.com/api/customers/upload/",
            $url,
            "test that the dev url matches"
        );

        /** @var Varien_Http_Client $client */
        $client = $this->helper->getApiClient($url, $custobarJson1);

        $request = $client->request();

        $this->assertEquals(
            200,
            $request->getStatus(),
            "is the response code 200"
        );
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     */
    public function testSettings()
    {
        /** @var Mage_Catalog_Model_Product $product */
        $product = Mage::getModel("catalog/product")->load(22111);

        $this->assertNotFalse(
            $this->helper->getEvenObjectTrackedModel(
                $product
            ),
            "Test can we track product models"
        );

        $this->assertNotFalse($this->helper->getApiKey());

        Mage::app()->getStore()
            ->setConfig("custobar/custobar_custoconnector/mode", 0);

        // test the api connection settings
        $this->assertEquals(
            "https://dev.custobar.com/api/",
            $this->helper->getApiBaseUri(),
            "Test do we get the live api base uri"
        );

        // unit tests seem to run mainly on the admin
        Mage::app()->getStore()
            ->setConfig("custobar/custobar_custoconnector/mode", 1);

        $this->assertEquals(
            "https://dev.custobar.com/api/",
            $this->helper->getApiBaseUri(),
            "Test the dev api base uri"
        );

        Mage::app()->getStore()
            ->setConfig(
                "custobar/custobar_custoconnector/tracking_script",
                <<<text
        (function(c,u,s,t,o,b,a,r) {var e;c[o]=[];c[b] = a;e =
u.createElement(s);r=u.getElementsByTagName(s)[0];e.async=1;e.src =
t;r.parentNode.insertBefore(e, r);})(window, document, 'script',
'https://www.custobar.com/js/v1/custobar.js', 'cstbr', 'cstbrConfig',
{'_companyToken':'ABCD1234','_pages':[['Login','^login/'],['Register','^register/']],'_banners':[['Sidebanner','#sidebar']]});
text
            );

        $this->assertContains(
            "'https://www.custobar.com/js/v1/custobar.js', 'cstbr', 'cstbrConfig'",
            $this->helper->getTrackingScript()
        );

        $this->assertTrue($this->helper->getIsWebsiteAllowed(3));
        $this->assertFalse($this->helper->getIsWebsiteAllowed(3000));

        Mage::app()->getStore()
            ->setConfig(
                "custobar/custobar_custoconnector/allowed_websites",
                ""
            );
        $this->assertFalse($this->helper->getIsWebsiteAllowed(3));
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     */
    public function testClient()
    {
        $params = array(
            "entity_id" => 1,
            "entity_type" => "Mage_Customer_Model_Customer"
        );

        Mage::app()->getStore()
            ->setConfig("custobar/custobar_custoconnector/mode", 0);

        /** @var Varien_Http_Client $client */
        $client = $this->helper->getApiClient(
            $this->helper->getApiBaseUri(),
            $params
        );

        $this->assertEquals(
            "https://dev.custobar.com:443/api/",
            $client->getUri(true)
        );
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     * @loadFixture orders.yaml
     */
    public function testFailingApiUrl()
    {
        Mage::app()->getStore()
            ->setConfig(
                "custobar/custobar_custoconnector/prefix",
                "prefixthatdoesntexists"
            );

        /** @var Custobar_CustoConnector_Model_Mysql4_Schedule_Collection $collection */
        $collection = Mage::getSingleton("custoconnector/schedule")
            ->getCollection();
        $this->assertCount(3, $collection->getData());

        $this->helper->sendUpdateRequests();
        sleep(1);
        $collection->resetData()->clear();

        $this->assertCount(
            6,
            $collection->getData(),
            "Do we have new rows for same items"
        );

        // we need to sleep to simulate that the cron run per "minute"
        // so we don`t get a constrain on adding the same entity with the same processed time
        sleep(1);

        $this->helper->sendUpdateRequests();
        sleep(1);
        $this->helper->sendUpdateRequests();
        sleep(1);
        $this->helper->sendUpdateRequests();

        $collection->resetData()->clear();

        $this->assertCount(
            15,
            $collection->getData(),
            "Do we have new rows for same items"
        );

        $collection->resetData()->clear();
        $collection->addOnlyErroneousFilter();

        $this->assertCount(3, $collection->getData());

        $errorIncrement = 0;

        /** @var Custobar_CustoConnector_Model_Schedule $item */
        foreach ($collection as $item) {
            $errorIncrement += $item->getErrorsCount();
        }

        $this->assertEquals(12, $errorIncrement);
    }

    /**
     * @test
     * @loadFixture testCleaning
     */
    public function testCleaning()
    {
        /** @var Custobar_CustoConnector_Model_Mysql4_Schedule_Collection $schedules */
        $schedules = Mage::getSingleton("custoconnector/schedule")
            ->getCollection()
            ->setPageSize(Custobar_CustoConnector_Helper_Data::$ITEMS_PER_CRON)
            ->setCurPage(1);

        $data = $schedules->getData();

        $this->assertCount(4, $data);

        /** @var Custobar_CustoConnector_Model_Schedule $model */
        $model = Mage::getModel("custoconnector/schedule");
        $model->cleanSchedules();

        $schedules->resetData();

        $data = $schedules->getData();

        $this->assertCount(2, $data);
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     */
    public function testMappingStore()
    {
        $this->clearSchedules();

        $storeViewBooksEng = Mage::app()->getStore(21);
        $mappedStoreBooksEng =
            $this->helper->getMappedEntity($storeViewBooksEng);
        $this->assertEquals(21, $mappedStoreBooksEng->getData("external_id"));
        $this->assertEquals(
            "Books Website, Books Store, English",
            $mappedStoreBooksEng->getData("name")
        );

        $storeViewBooksGerman = Mage::app()->getStore(22);
        $mappedStoreBooksGerman =
            $this->helper->getMappedEntity($storeViewBooksGerman);
        $this->assertEquals(
            22,
            $mappedStoreBooksGerman->getData("external_id")
        );
        $this->assertEquals(
            "Books Website, Books Store, German",
            $mappedStoreBooksGerman->getData("name")
        );

        $storeViewExtensionsEng = Mage::app()->getStore(31);
        $mappedStoreExtensionsEng =
            $this->helper->getMappedEntity($storeViewExtensionsEng);
        $this->assertEquals(
            31,
            $mappedStoreExtensionsEng->getData("external_id")
        );
        $this->assertEquals(
            "Extensions Website, Extensions Store, English",
            $mappedStoreExtensionsEng->getData("name")
        );
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     */
    public function testSendingStoreData()
    {
        $this->clearSchedules();
        $this->helper->scheduleUpdate(
            21,
            null,
            "Mage_Core_Model_Store"
        );

        $schedulesBefore = $this->getAllSchedules();

        $schedulesBefore->addFieldToFilter(
            "entity_type",
            "Mage_Core_Model_Store"
        );

        $schedulesBeforeData = $schedulesBefore->getData();

        $this->assertEquals(
            "Mage_Core_Model_Store",
            $schedulesBeforeData[0]["entity_type"]
        );

        Mage::app()->getStore()
            ->setConfig("custobar/custobar_custoconnector/mode", 1);

        $clientMock = $this->mockHttpClientReturnOk();
        $this->helper->setClient($clientMock);
        $this->helper->sendUpdateRequests();

        $schedulesAfter = $this->getAllUnProcessedSchedules()->getData();

        $this->assertCount(
            0,
            $schedulesAfter,
            "All schedules have been processed"
        );
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     * @loadFixture newsletter.yaml
     */
    public function testMappingNewsLetter()
    {
        $this->clearSchedules();

        /** @var Mage_Newsletter_Model_Subscriber $newsletterSubscriberJohnDoe */
        $newsletterSubscriberJohnDoe =
            Mage::getModel("Mage_Newsletter_Model_Subscriber")->load(13);

        $mappedSubscriberJohnDoe =
            $this->helper->getMappedEntity($newsletterSubscriberJohnDoe);

        self::assertEquals("john@doe.com", $mappedSubscriberJohnDoe["email"]);
        self::assertEquals("1", $mappedSubscriberJohnDoe["customer_id"]);
        self::assertEquals("MAIL_SUBSCRIBE", $mappedSubscriberJohnDoe["type"]);
        self::assertEquals("31", $mappedSubscriberJohnDoe["store_id"]);

        $newsletterSubscriberJohnDoe->setStatus(
            Mage_Newsletter_Model_Subscriber::STATUS_UNSUBSCRIBED
        );

        $mappedSubscriberJohnDoe =
            $this->helper->getMappedEntity($newsletterSubscriberJohnDoe);

        self::assertEquals(
            "MAIL_UNSUBSCRIBE",
            $mappedSubscriberJohnDoe["type"]
        );

        /** @var Mage_Newsletter_Model_Subscriber $newsletterSubscriberJohnDoe */
        $newsletterSubscriberJojo =
            Mage::getModel("Mage_Newsletter_Model_Subscriber")->load(14);

        $mappedSubscriberJoJo =
            $this->helper->getMappedEntity($newsletterSubscriberJojo);

        self::assertEquals("MAIL_UNSUBSCRIBE", $mappedSubscriberJoJo["type"]);
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     * @loadFixture newsletter.yaml
     */
    public function testSendingNewsLetterData()
    {
        $this->helper->scheduleUpdate(
            12,
            21,
            "Mage_Newsletter_Model_Subscriber"
        );

        $schedulesBefore = $this->getAllSchedules();

        $schedulesBefore->addFieldToFilter(
            "entity_type",
            "Mage_Newsletter_Model_Subscriber"
        );

        $schedulesBeforeData = $schedulesBefore->getData();

        $this->assertEquals(
            "Mage_Newsletter_Model_Subscriber",
            $schedulesBeforeData[0]["entity_type"]
        );

        Mage::app()->getStore()
            ->setConfig("custobar/custobar_custoconnector/mode", 1);

        $this->helper->sendUpdateRequests();

        $schedulesAfter = $this->getAllUnProcessedSchedules()->getData();

        $this->assertCount(
            0,
            $schedulesAfter,
            "All schedules have been processed"
        );
    }

    /**
     * Get object created at date affected current active store timezone
     *
     * @param string $createdAt
     *
     * @return \Zend_Date
     */
    public function getCreatedAtDateAsIsoISO8601($createdAt)
    {
        return Mage::app()->getLocale()->date(
            Varien_Date::toTimestamp($createdAt),
            null,
            null,
            true
        )->toString(Zend_Date::ISO_8601);
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     */
    public function testMappingCustomer()
    {
        $this->clearSchedules();

        /** @var Mage_Customer_Model_Customer $john */
        $john = Mage::getModel("customer/customer")->load(1);
        $customerJohn = $this->helper->getMappedEntity($john);

        $this->assertEquals(1, $customerJohn->getData("external_id"));
        $this->assertEquals("John", $customerJohn->getData("first_name"));
        $this->assertEquals("Doe", $customerJohn->getData("last_name"));
        $this->assertEquals(
            "Address 123",
            $customerJohn->getData("street_address"),
            "is address present"
        );

        $this->assertEquals(
            "US",
            $customerJohn->getData("country"),
            "Is the country id present"
        );

        $this->assertEquals(
            "31",
            $customerJohn->getData("store_id"),
            "Is the store id present"
        );

        $this->assertEquals(
            "89001",
            $customerJohn->getData("zip_code"),
            "Is the zip code present"
        );

        $this->assertEquals(
            "Alamo",
            $customerJohn->getData("city"),
            "Is the city present"
        );

        $this->assertEquals(
            "555-55-55",
            $customerJohn->getData("phone_number"),
            "Is the phone present"
        );

        $this->assertEquals(
            "2016-08-15T10:02:52+00:00",
            $customerJohn->getData("date_joined"),
            "Is the date joined present"
        );

        /** @var Mage_Customer_Model_Customer $jane */
        $jane = Mage::getModel("customer/customer")->load(2);
        $customerJane = $this->helper->getMappedEntity($jane);

        $this->assertEquals(
            "21",
            $customerJane->getData("store_id"),
            "Is the store id present"
        );
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     */
    public function testSendingCustomerData()
    {
        $this->clearSchedules();

        $this->helper->scheduleUpdate(
            1,
            31,
            "Mage_Customer_Model_Customer"
        );

        $schedulesBefore = $this->getAllSchedules();

        $schedulesBefore->addFieldToFilter(
            "entity_type",
            "Mage_Customer_Model_Customer"
        );

        $schedulesBeforeData = $schedulesBefore->getData();

        $this->assertEquals(
            "Mage_Customer_Model_Customer",
            $schedulesBeforeData[0]["entity_type"]
        );

        Mage::app()->getStore()
            ->setConfig("custobar/custobar_custoconnector/mode", 1);

        $this->helper->sendUpdateRequests();

        $schedulesAfter = $this->getAllUnProcessedSchedules()->getData();

        $this->assertCount(
            0,
            $schedulesAfter,
            "All schedules have been processed"
        );
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     * @loadFixture image.yaml
     */
    public function testMappingProduct()
    {
        $this->clearSchedules();
        $scheduleDefault = $this->helper->scheduleUpdate(
            22201,
            22,
            "Mage_Catalog_Model_Product"
        );

        $scheduleUkr = $this->helper->scheduleUpdate(
            22201,
            23,
            "Mage_Catalog_Model_Product"
        );

        $scheduleNotVisible = $this->helper->scheduleUpdate(
            22202,
            22,
            "Mage_Catalog_Model_Product"
        );

        $schedules = $this->getAllSchedules();

        $this->assertCount(
            3,
            $schedules->getData(),
            "Assert that we can have two of the same types in the schedule as the store differs. Also one for not visible"
        );

        $entity = $this->helper->getEntityFromSchedule($scheduleDefault);
        $mappedProduct = $this->helper->getMappedEntity($entity);

        $this->assertEquals(
            "wells-0002",
            $mappedProduct->getData("external_id"),
            "Product external_id present"
        );

        $this->assertEquals(
            99,
            $mappedProduct->getData("price"),
            "Product price present"
        );

        $this->assertEquals(
            99,
            $mappedProduct->getData("minimal_price"),
            "Product minimal_price present"
        );

        $this->assertEquals(
            "simple",
            $mappedProduct->getData("mage_type"),
            "Product mage_type present"
        );

        $this->assertEquals(
            "Default",
            $mappedProduct->getData("type"),
            "Product type present"
        );

        $this->assertEquals(
            "Zeitreisen",
            $mappedProduct->getData("category"),
            "Product category present"
        );
        $this->assertEquals(
            "222",
            $mappedProduct->getData("category_id"),
            "Product category_id present"
        );
        $this->assertEquals(
            "Wells, H.G. 1898. The Time Machine",
            $mappedProduct->getData("title"),
            "Product title present"
        );
        $this->assertEquals(
            "http://magento.dev/media/catalog/product/p/m/pms005a_4.jpg",
            $mappedProduct->getData("image"),
            "Product image present"
        );
        $this->assertEquals(
            "http://magento.dev/index.php/testniceurl.html/?___store=books_deu",
            $mappedProduct->getData("url"),
            "Product default url present"
        );
        $this->assertEquals(
            "0",
            $mappedProduct->getData("sale_price"),
            "Product sale_price present"
        );
        $this->assertEquals(
            "Wells, H.G. 1898. The Time Machine",
            $mappedProduct->getData("description"),
            "Product description present"
        );

        $this->assertEquals(
            22,
            $mappedProduct->getData("store_id"),
            "Product german store id present"
        );

        $this->assertEquals(
            "de",
            $mappedProduct->getData("language"),
            "Product german language present"
        );

        $entityUkr = $this->helper->getEntityFromSchedule($scheduleUkr);
        $mappedProductUkr = $this->helper->getMappedEntity($entityUkr);

        $this->assertEquals(
            "Герберт Джордж Уеллс: Машина часу",
            $mappedProductUkr->getData("title"),
            "Product ukr title present"
        );

        $this->assertEquals(
            "Герберт Джордж Уеллс: Машина часу desc",
            $mappedProductUkr->getData("description"),
            "Product ukr description present"
        );

        $this->assertEquals(
            23,
            $mappedProductUkr->getData("store_id")
        );

        $this->assertEquals(
            "uk",
            $mappedProductUkr->getData("language")
        );

        $this->assertEquals(
            "http://magento.dev/index.php/testniceurlurk.html/?___store=books_ukr",
            $mappedProductUkr->getData("url"),
            "Product ukr url present"
        );

        $this->assertEquals(
            "Подорожі у часі",
            $mappedProductUkr->getData("category"),
            "Product ukr category present"
        );


        $mappedNotVisible = $this->helper->getMappedEntity(
            $this->helper->getEntityFromSchedule($scheduleNotVisible)
        );

        $this->assertArrayNotHasKey(
            "url",
            $mappedNotVisible->getData(),
            "Missing url key as its not visible by it self"
        );
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     */
    public function testMappingConfigurableProduct()
    {
        $this->clearSchedules();

        $entity = Mage::getModel("catalog/product")->load(22204);
        $mappedProduct = $this->helper->getMappedEntity(
            $entity
        );

        $this->assertEquals(
            "booksimple-configurable",
            $mappedProduct->getData("external_id"),
            "Product external_id present"
        );

        $this->assertEquals(
            "booksimple-red,booksimple-blue",
            $mappedProduct->getData("mage_child_ids"),
            "Product child ids present"
        );

        $mappedChildProduct = $this->helper->getMappedEntity(
            Mage::getModel("catalog/product")->load(22203)
        );

        $this->assertEquals(
            "booksimple-blue",
            $mappedChildProduct->getData("external_id"),
            "Product external_id present"
        );

        $this->assertEquals(
            "booksimple-configurable,booksimple-bundle,booksimple-grouped",
            $mappedChildProduct->getData("mage_parent_ids"),
            "Product parent ids present"
        );
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     */
    public function testMappingBundleProduct()
    {
        $this->clearSchedules();

        $entity = Mage::getModel("catalog/product")->load(22205);
        $mappedProduct = $this->helper->getMappedEntity(
            $entity
        );

        $this->assertEquals(
            "booksimple-bundle",
            $mappedProduct->getData("external_id"),
            "Product external_id present"
        );

        $this->assertEquals(
            "booksimple-red,booksimple-blue",
            $mappedProduct->getData("mage_child_ids"),
            "Product child ids present"
        );

        $mappedChildProduct = $this->helper->getMappedEntity(
            Mage::getModel("catalog/product")->load(22203)
        );

        $this->assertEquals(
            "booksimple-blue",
            $mappedChildProduct->getData("external_id"),
            "Product external_id present"
        );

        $this->assertEquals(
            "booksimple-configurable,booksimple-bundle,booksimple-grouped",
            $mappedChildProduct->getData("mage_parent_ids"),
            "Product parent ids present"
        );
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     */
    public function testMappingGroupedProduct()
    {
        $this->clearSchedules();

        $entity = Mage::getModel("catalog/product")->load(22206);
        $mappedProduct = $this->helper->getMappedEntity(
            $entity
        );

        $this->assertEquals(
            "booksimple-grouped",
            $mappedProduct->getData("external_id"),
            "Product external_id present"
        );

        $this->assertContains(
            "wells-0002",
            $mappedProduct->getData("mage_child_ids"),
            "Product child ids present"
        );
        $this->assertContains(
            "booksimple-red",
            $mappedProduct->getData("mage_child_ids"),
            "Product child ids present"
        );
        $this->assertContains(
            "booksimple-blue",
            $mappedProduct->getData("mage_child_ids"),
            "Product child ids present"
        );

        $mappedChildProduct = $this->helper->getMappedEntity(
            Mage::getModel("catalog/product")->load(22203)
        );

        $this->assertEquals(
            "booksimple-blue",
            $mappedChildProduct->getData("external_id"),
            "Product external_id present"
        );

        $this->assertEquals(
            "booksimple-configurable,booksimple-bundle,booksimple-grouped",
            $mappedChildProduct->getData("mage_parent_ids"),
            "Product parent ids present"
        );
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     */
    public function testMappingDropdownAttributeProduct()
    {
        $this->markTestIncomplete(
            "Need to figure out how to add select options to the fixture"
        );

        /** @var \Mage_Catalog_Model_Product $product */
        $product = Mage::getModel("catalog/product")->load(22202);

        $mappedProduct = $this->helper->getMappedEntity(
            $product
        );

        $this->assertEquals(
            "booksimple-red",
            $mappedProduct->getData("external_id"),
            "Product external_id present"
        );

        $this->assertEquals(
            "option 2 label",
            $mappedProduct->getData("brand"),
            "Product attribute present"
        );
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     */
    public function testProductMissingInDatabase()
    {
        $this->clearSchedules();

        $scheduleDefault = $this->helper->scheduleUpdate(
            123123132,
            22,
            "Mage_Catalog_Model_Product"
        );
        $this->helper->sendUpdateRequests();
        sleep(1); // fixes issue with the processed time being the same
        for ($i = 0; $i < 2; $i++) {
            $this->helper->sendUpdateRequests();
            sleep(1); // fixes issue with the processed time being the same
        }
        $errorLogContents = file_get_contents($this->custobarLogFile());
        $expectedErrorMessage =
            "CustoConnector: Could not find 123123132 of Mage_Catalog_Model_Product. Deleted or data missing.";
        $this->assertContains(
            $expectedErrorMessage,
            $errorLogContents
        );

        $this->assertEquals(
            1,
            substr_count($errorLogContents, $expectedErrorMessage)
        );
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     */
    public function testSendingProductData()
    {
        $this->clearSchedules();
        $scheduleDefault = $this->helper->scheduleUpdate(
            22201,
            22,
            "Mage_Catalog_Model_Product"
        );

        $scheduleUkr = $this->helper->scheduleUpdate(
            22201,
            23,
            "Mage_Catalog_Model_Product"
        );
        $schedulesBefore = $this->getAllSchedules()->getData();

        $this->assertCount(
            2,
            $schedulesBefore,
            "is only one schedule present"
        );

        Mage::app()->getStore()
            ->setConfig("custobar/custobar_custoconnector/mode", 1);

        $this->helper->sendUpdateRequests();

        $schedulesAfter = $this->getAllUnProcessedSchedules()->getData();

        $this->assertCount(
            0,
            $schedulesAfter,
            "All schedules have been processed"
        );
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     * @loadFixture orders
     */
    public function testMappingOrderWithConfigurableProducts()
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = Mage::getModel("sales/order")->load(122);
        $mappedOrder = $this->helper->getMappedEntity($order);

        /** @var array $items */
        $items = $mappedOrder->getData('magento__items');
        $correct_items = [
            'item#321' => [
                'sku' => 'msj012',
                'unit_price' => 45500,
                'quantity' => '15.0000',
                'discount' => 136500,
                'total' => 546000
            ],
            'item#323' => [
                'sku' => 'msj009',
                'unit_price' => 51000,
                'quantity' => '15.0000',
                'discount' => 153000,
                'total' => 612000
            ],
            'item#325' => [
                'sku' => 'wbk009',
                'unit_price' => 18500,
                'quantity' => '15.0000',
                'discount' => 55500,
                'total' => 222000
            ],
            'item#327' => [
                'sku' => 'hdd000',
                'unit_price' => 11000,
                'quantity' => '7.0000',
                'discount' => 15400,
                'total' => 61600
            ]
        ];
        $this->assertCount(count($correct_items), $items);
        foreach ($items as $item) {
            $key = "item#{$item['external_id']}";
            $this->assertTrue(isset($correct_items[$key]));
            $correct = $correct_items[$key];
            $this->assertEquals(
                100000135,
                $item['sale_external_id'],
                "Order ID for {$key}"
            );
            $this->assertEquals(
                $correct['sku'],
                $item['product_id'],
                "SKU for {$key}"
            );
            $this->assertEquals(
                $correct['unit_price'],
                $item['unit_price'],
                "Unit price for {$key}"
            );
            $this->assertEquals(
                $correct['quantity'],
                $item['quantity'],
                "Quantity for {$key}"
            );
            $this->assertEquals(
                $correct['discount'],
                $item['discount'],
                "Discount for {$key}"
            );
            $this->assertEquals(
                $correct['total'],
                $item['total'],
                "Total price for {$key}"
            );
        }
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     * @loadFixture orders
     */
    public function testOrderCustobarJson()
    {
        /** @var Mage_Sales_Model_Order $order */
        $order1 = Mage::getModel("sales/order")->load(122);
        $order2 = Mage::getModel("sales/order")->load(16);

        $json = $this->helper->getCustobarJsonForEntity(
            [
                $order1,
                $order2
            ]
        );

        $data = json_decode($json, true);

        $this->assertCount(
            6,
            $data["sales"],
            "are there correct amount of rows"
        );

        $this->assertEquals(100000135, $data["sales"][0]["sale_external_id"]);
        $this->assertEquals(
            "dong@example.com",
            $data["sales"][0]["sale_email"]
        );
        $this->assertEquals(321, $data["sales"][0]["external_id"]);
        $this->assertEquals(15, $data["sales"][0]["quantity"]);


        $this->assertEquals(323, $data["sales"][1]["external_id"]);
        $this->assertEquals(100000135, $data["sales"][1]["sale_external_id"]);

        $this->assertEquals(100000135, $data["sales"][2]["sale_external_id"]);
        $this->assertEquals(325, $data["sales"][2]["external_id"]);

        $this->assertEquals(100000135, $data["sales"][3]["sale_external_id"]);
        $this->assertEquals(327, $data["sales"][3]["external_id"]);

        $this->assertEquals("000000013", $data["sales"][4]["sale_external_id"]);
        $this->assertEquals("john@doe.com", $data["sales"][4]["sale_email"]);
        $this->assertEquals(16, $data["sales"][4]["external_id"]);
        $this->assertEquals(1, $data["sales"][4]["quantity"]);

        $this->assertEquals("000000013", $data["sales"][5]["sale_external_id"]);
        $this->assertEquals(17, $data["sales"][5]["external_id"]);
    }


    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     * @loadFixture orders
     */
    public function testProductCustobarJson()
    {

        /** @var Mage_Catalog_Model_Product $product */
        $product1 = Mage::getModel("catalog/product")->load(22201);
        $product2 = Mage::getModel("catalog/product")->load(31002);

        $json = $this->helper->getCustobarJsonForEntity(
            [
                $product1,
                $product2
            ]
        );

        $data = json_decode($json, true);

        $this->assertCount(
            2,
            $data["products"],
            "are there correct amount of rows"
        );

        $this->assertEquals("wells-0002", $data["products"][0]["external_id"]);
        $this->assertEquals(
            "extension-0002",
            $data["products"][1]["external_id"]
        );
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     * @loadFixture orders
     */
    public function testMappingOrderWithSimpleProducts()
    {
        $this->clearSchedules();

        /** @var Mage_Sales_Model_Order $order */
        $order = Mage::getModel("sales/order")->load(16);

        $mappedOrder = $this->helper->getMappedEntity($order);

        $this->assertEquals(
            "000000013",
            $mappedOrder->getData("sale_external_id"),
            "Order sale_external_id present"
        );

        $this->assertEquals(
            "COMPLETE",
            $mappedOrder->getData("sale_state"),
            "Order state is present"
        );

        $this->assertEquals(
            1,
            $mappedOrder->getData("sale_customer_id"),
            "Order sale_customer_id present"
        );

        $this->assertEquals(
            "john@doe.com",
            $mappedOrder->getData("sale_email"),
            "Order sale_email present"
        );

        $this->assertEquals(
            "2016-08-15T02:28:28-07:00",
            $mappedOrder->getData("sale_date"),
            "Order sale_date present"
        );

        $this->assertEquals(
            31,
            $mappedOrder->getData("sale_shop_id"),
            "Order sale_shop_id present"
        );
        $this->assertEquals(
            "0",
            $mappedOrder->getData("sale_discount"),
            "Order sale_discount present"
        );
        $this->assertEquals(
            500,
            $mappedOrder->getData("sale_total"),
            "Order sale_total present"
        );
        $this->assertEquals(
            "checkmo",
            $mappedOrder->getData("sale_payment_method"),
            "Order sale_payment_method present"
        );

        // Order item tests start here
        $orderItems = $mappedOrder->getData("magento__items");

        $this->assertEquals(
            "000000013",
            $orderItems[0]["sale_external_id"],
            "Order row 1 external_id present"
        );

        $this->assertEquals(
            16,
            $orderItems[0]["external_id"],
            "Order row 1 external_id present"
        );
        $this->assertEquals(
            "yanukovych-0001",
            $orderItems[0]["product_id"],
            "Order row 1 product_id present"
        );
        $this->assertEquals(
            99,
            $orderItems[0]["unit_price"],
            "Order row 1 unit_price present"
        );
        $this->assertEquals(
            1,
            $orderItems[0]["quantity"],
            "Order row 1 quantity present"
        );
        $this->assertEquals(
            99,
            $orderItems[0]["total"],
            "Order row 1  total present"
        );

        $this->assertEquals(
            "000000013",
            $orderItems[1]["sale_external_id"],
            "Order row 1 external_id present"
        );

        $this->assertEquals(
            17,
            $orderItems[1]["external_id"],
            "Order row 1 external_id present"
        );
        $this->assertEquals(
            "wells-0001",
            $orderItems[1]["product_id"],
            "Order row 1 product_id present"
        );
        $this->assertEquals(
            99,
            $orderItems[1]["unit_price"],
            "Order row 1 unit_price present"
        );
        $this->assertEquals(
            1,
            $orderItems[1]["quantity"],
            "Order row 1 quantity present"
        );
        $this->assertEquals(
            99,
            $orderItems[1]["total"],
            "Order row 1  total present"
        );
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     * @loadFixture orders
     */
    public function testSendingOrderWithSimpleProductsData()
    {
        $this->clearSchedules();

        $this->helper->scheduleUpdate(
            16,
            31,
            "Mage_Sales_Model_Order"
        );

        $schedulesBefore = $this->getAllSchedules();

        $schedulesBefore->addFieldToFilter(
            "entity_type",
            "Mage_Sales_Model_Order"
        );

        $schedulesBeforeData = $schedulesBefore->getData();
        $this->assertCount(1, $schedulesBefore);
        $this->assertEquals(
            "Mage_Sales_Model_Order",
            $schedulesBeforeData[0]["entity_type"]
        );

        Mage::app()->getStore()
            ->setConfig("custobar/custobar_custoconnector/mode", 1);

        $this->helper->sendUpdateRequests();

        $schedulesAfter = $this->getAllUnProcessedSchedules()->getData();

        $this->assertCount(
            0,
            $schedulesAfter,
            "All schedules have been processed"
        );
    }


    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     * @loadFixture orders
     */
    public function testInitialTableFunction()
    {
        $this->clearSchedules();
        $this->cleanInitials();

        Custobar_CustoConnector_Helper_Data::$INITIAL_PAGE_SIZE = 2;

        // we can call the method two time without an exception
        $this->helper->startInitialScheduling();
        $this->helper->startInitialScheduling();

        /** @var Custobar_CustoConnector_Model_Initial $model */
        $model = Mage::getModel("custoconnector/initial");

        /** @var Custobar_CustoConnector_Model_Mysql4_Initial_Collection $collection */
        $collection = $model->getCollection();

        $collectionData = $collection->getData();
        self::assertCount(
            4,
            $collectionData,
            "Assert that we have the right amount initial chedules present"
        );

        self::assertEquals(6, $collectionData[0]["pages"], "Product pages");
        self::assertEquals(0, $collectionData[0]["page"]);
        self::assertEquals(
            "Mage_Catalog_Model_Product",
            $collectionData[0]["entity_type"]
        );

        self::assertEquals(1, $collectionData[1]["pages"], "Customer pages");
        self::assertEquals(0, $collectionData[1]["page"]);
        self::assertEquals(
            "Mage_Customer_Model_Customer",
            $collectionData[1]["entity_type"]
        );

        self::assertEquals(1, $collectionData[2]["pages"]);
        self::assertEquals(0, $collectionData[2]["page"]);
        self::assertEquals(
            "Mage_Sales_Model_Order",
            $collectionData[2]["entity_type"]
        );

        self::assertEquals(4, $collectionData[3]["pages"], "Store pages");
        self::assertEquals(0, $collectionData[3]["page"]);
        self::assertEquals(
            "Mage_Core_Model_Store",
            $collectionData[3]["entity_type"]
        );

        /** @var array $types */
        $types = $collection->getColumnValues("entity_type");

        self::assertFalse(
            in_array("Mage_Newsletter_Model_Subscriber", $types),
            "Mage_Newsletter_Model_Subscriber is not present as no data in the table"
        );

        $trackedModels = array_keys($this->helper->getConfigTrackedModels());
        $pages = 0;
        foreach ($trackedModels as $trackedModel) {
            // skip internal models as they dont appear in the schedule table
            if ($this->helper->getIsModelInternal($trackedModel)) {
                continue;
            }
            $expectedCollection =
                Mage::getSingleton($trackedModel)->getCollection();
            /** @var Mage_Core_Model_Abstract $expectedEntity */
            $pages += (int)ceil($expectedCollection->count() / 2);
        }

        for ($i = 0; $i < $pages; $i++) {
            $this->helper->handleNextInitialPage();
        }

        $schedules = $this->getAllSchedules();
        $schedulesData = $schedules->getData();
        $adminStoreCount = 1;
        $storeCount = 6; // Website#2: 3, Website#3: 3
        $customerCount = 2; // W2: 1, W3: 1
        $orderCount = 2;
        $productCount = 3 * 10 + 3
            * 3;

        // PRODUCTS:
        //  stores:
        //      W2:
        //          3
        //      W3:
        //          3
        //      products:
        //          W2: 11,
        //          W3: 3
        self::assertCount(
            $adminStoreCount + $storeCount + $customerCount + $orderCount
            + $productCount,
            $schedulesData,
            "does the combined amount of items match the scheduled ones"
        );

        $cronSchedules = $this->helper->getCronSchedulesByCode(
            Custobar_CustoConnector_Helper_Data::$INITIAL_CRON_CODE
        );

        self::assertCount(
            $pages + 1,
            $cronSchedules->getData(),
            "Does the cron event match the tracked models count + the creep "
        );

        $collection->clear();
        $collection->resetData();

        $initialCollectionDataAfter = $collection->getData();

        self::assertCount(
            4,
            $initialCollectionDataAfter,
            "Does the initial table have rows"
        );

        $this->helper->handleNextInitialPage();

        $collection->clear();
        $collection->resetData();

        $initialCollectionDataAfter = $collection->getData();

        self::assertCount(
            0,
            $initialCollectionDataAfter,
            "Is the initial table empty after we call handle once more"
        );

        $model->cleanInitials();

        /** @var Custobar_CustoConnector_Model_Mysql4_Initial_Collection $collection */
        $collection = $model->getCollection();

        self::assertCount(0, $collection->getData());
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     * @loadFixture orders.yaml
     */
    public function testScheduleItemsWebsitePrevention()
    {
        Mage::app()->getStore()
            ->setConfig(
                "custobar/custobar_custoconnector/allowed_websites",
                "2"
            );

        // this is in the allowed websites
        $this->helper->scheduleUpdate(
            22111,
            21,
            "Mage_Catalog_Model_Product"
        );

        // this isnt in the allowed website
        $this->helper->scheduleUpdate(
            31002,
            31,
            "Mage_Catalog_Model_Product"
        );

        $schedules = $this->getAllUnProcessedSchedules();

        $itemsForProcessing = [];

        /** @var Custobar_CustoConnector_Model_Schedule $schedule */
        foreach ($schedules as $schedule) {
            $entity = $this->helper->getEntityFromSchedule($schedule);
            $this->helper->returnItemForProcessing(
                $entity,
                $schedule,
                $itemsForProcessing
            );
        }

        // we have 1 customer that is allowed from the default fixture
        // and only 1 product that comes form here
        $this->assertCount(2, $itemsForProcessing["entities"]);
        $this->assertCount(2, $itemsForProcessing["schedules"]);

        $this->assertTrue(
            is_a(
                $itemsForProcessing["entities"][0],
                "Mage_Customer_Model_Customer"
            )
        );
        $this->assertTrue(
            is_a(
                $itemsForProcessing["entities"][1],
                "Mage_Catalog_Model_Product"
            )
        );

        $this->assertEquals(22111, $itemsForProcessing["entities"][1]->getId());
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     * @loadFixture errors
     */
    public function testAddingScheduleErrorMaximum()
    {

        /** @var Custobar_CustoConnector_Model_Schedule $first */
        $schedules = $this->getAllUnProcessedSchedules();
        $first = $schedules->getFirstItem();
        $first->setProcessedAt(Varien_Date::formatDate(true));
        $first->setErrors(Custobar_CustoConnector_Helper_Data::MAX_ERROR_COUNT);
        $first->save();

        /** @var Custobar_CustoConnector_Model_Schedule $last */
        $last = $schedules->getLastItem();
        $last->setProcessedAt(Varien_Date::formatDate(true));
        $last->save();

        $this->helper->rescheduleFailedSchedule($first, "key");
        $this->helper->rescheduleFailedSchedule($last, "key");

        $schedules = $this->getAllUnProcessedSchedules();

        $this->assertCount(5, $schedules->getData());

        $correctErrorCount = $this->getAllSchedules();
        $correctErrorCount->getSelect()->where("errors = 1001");

        /** @var Custobar_CustoConnector_Model_Schedule $errorCountSchedule */
        $errorCountSchedule = $correctErrorCount->getFirstItem();

        self::assertEquals(1001, $errorCountSchedule->getErrorsCount());


        $maxErrorCount = $this->getAllSchedules();
        $maxErrorCount->getSelect()->where(
            "errors > " . (Custobar_CustoConnector_Helper_Data::MAX_ERROR_COUNT
                - 1)
        );

        self::assertCount(1, $maxErrorCount->getData());
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     * @loadFixture orders
     */
    public function testInitialWithSpecificModel()
    {
        $this->clearSchedules();

        Custobar_CustoConnector_Helper_Data::$INITIAL_PAGE_SIZE = 1;

        // we can call the method two time without an exception
        $this->helper->startInitialScheduling("Mage_Customer_Model_Customer");

        /** @var Custobar_CustoConnector_Model_Initial $model */
        $model = Mage::getModel("custoconnector/initial");

        /** @var Custobar_CustoConnector_Model_Mysql4_Initial_Collection $collection */
        $collection = $model->getCollection();

        $collectionData = $collection->getData();
        self::assertCount(1, $collectionData);

        self::assertEquals(2, $collectionData[0]["pages"], "Customer pages");
        self::assertEquals(0, $collectionData[0]["page"]);
        self::assertEquals(
            "Mage_Customer_Model_Customer",
            $collectionData[0]["entity_type"]
        );
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     * @loadFixture errors
     */
    public function testScheduleErrors()
    {
        /** @var Custobar_CustoConnector_Model_Mysql4_Schedule_Collection $schedulesCollection */
        $schedulesCollection = Mage::getSingleton("custoconnector/schedule")
            ->getCollection();

        $schedulesCollection->addOnlyErroneousFilter();
        $this->assertCount(
            2,
            $schedulesCollection->getData(),
            "Are there correct amount of Erroneous items"
        );
        $this->helper->sendUpdateRequests();

        $schedulesCollection->resetData()->clear();
        $schedulesCollection->getSelect()->reset('where');
        $schedulesCollection->getSelect()->where(
            "entity_type = 'MODEL_THAT_DOESNT_EXISTS'"
        );

        /** @var Custobar_CustoConnector_Model_Schedule $nonExistent */
        $nonExistent = $schedulesCollection->getFirstItem();

        self::assertFalse($nonExistent->isEmpty());

        $date = Varien_Date::formatDate(true, false);

        self::assertContains(
            $date,
            $nonExistent->getProcessedAt()
        );

        $schedulesCollection->resetData()->clear();
        $schedulesCollection->getSelect()->reset('where');
        $schedulesCollection->getSelect()->where(
            "entity_type = 'Mage_Customer_Model_Address'"
        );

        /** @var Custobar_CustoConnector_Model_Schedule $address */
        $address = $schedulesCollection->getFirstItem();

        self::assertFalse($address->isEmpty());
        self::assertContains(
            $date,
            $address->getProcessedAt()
        );

        $schedulesCollection->resetData()->clear();
        $schedulesCollection->getSelect()->reset('where');

        /** @var Custobar_CustoConnector_Model_Schedule $item */
        $errors = 0;
        foreach ($schedulesCollection as $item) {
            $errors += $item->getErrorsCount();

            $this->assertNotEquals(
                "0000-00-00 00:00:00",
                $item->getProcessedAt()
            );
        }

        self::assertEquals(9000, $errors, "Is the error count still the same");
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     */
    public function testMarkSchedulesWithDeletetItemsProcessed()
    {
        /** @var Custobar_CustoConnector_Model_Mysql4_Schedule_Collection $schedules */
        $schedules = $this->getAllUnProcessedSchedules();
        $this->assertCount(3, $schedules->getData());
        $schedules->resetData()->clear();

        /** @var Mage_Customer_Model_Customer $john */
        $john = Mage::getModel("customer/customer")->load(1);
        $john->delete();

        $this->assertCount(2, $schedules->getData());
        $schedules->resetData()->clear();

        $product = Mage::getModel('catalog/product')->load(22201);
        $product->setData("test_data", 1);
        $product->save();
        $this->assertCount(5, $schedules->getData());
        $schedules->resetData()->clear();

        $product->delete();
        $this->assertCount(2, $schedules->getData());
    }

    /**
     * @test
     * @loadFixture initial
     */
    public function testIsInitialRunning()
    {
        $this->assertTrue($this->helper->isInitialRunning());
        $this->cleanInitials();
        $this->assertFalse($this->helper->isInitialRunning());
    }

    protected function tearDown()
    {
        @session_destroy();
        parent::tearDown();
        $this->clearSchedules();
        $this->cleanInitials();
        $this->clearCustobarLog();
    }

    protected function clearSchedules()
    {
        /** @var Custobar_CustoConnector_Model_Mysql4_Schedule_Collection $schedules */
        $schedules = $this->getAllSchedules();

        /** @var Custobar_CustoConnector_Model_Schedule $schedule */
        foreach ($schedules as $schedule) {
            $schedule->delete();
        }
    }

    /**
     * @return \Custobar_CustoConnector_Model_Mysql4_Schedule_Collection
     */
    private function getAllSchedules()
    {
        /** @var Custobar_CustoConnector_Model_Mysql4_Schedule_Collection $schedules */
        $schedules = Mage::getSingleton("custoconnector/schedule")
            ->getCollection();
        return $schedules;
    }

    /**
     * @return \Custobar_CustoConnector_Model_Mysql4_Schedule_Collection
     */
    private function getAllUnProcessedSchedules()
    {
        /** @var Custobar_CustoConnector_Model_Mysql4_Schedule_Collection $schedules */
        $schedules = Mage::getSingleton("custoconnector/schedule")
            ->getCollection()
            ->setOrder("entity_id", "ASC")
            ->addOnlyForSendingFilter();

        return $schedules;
    }

    private function cleanInitials()
    {
        /** @var Custobar_CustoConnector_Model_Initial $model */
        $model = Mage::getModel("custoconnector/initial");
        $model->cleanInitials();
    }

    /**
     * @return string
     */
    protected function custobarLogFile()
    {
        $logDir = Mage::getBaseDir('var') . DS . 'log';
        $file = "custoconnector.log";
        $logFile = $logDir . DS . $file;
        return $logFile;
    }
}
