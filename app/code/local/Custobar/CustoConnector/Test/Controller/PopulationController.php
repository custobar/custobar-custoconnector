<?php

/**
 * Class Custobar_CustoConnector_Test_Controller_PopulationController
 *
 * @group Custobar_Controller
 */
class Custobar_CustoConnector_Test_Controller_PopulationController
    extends EcomDev_PHPUnit_Test_Case_Controller
{

    /**
     * @var Custobar_CustoConnector_Helper_Data
     */
    private $helper;

    protected function setUp()
    {
        parent::setUp();


        static $registry;

        if (!$registry) {
            $registry
                = EcomDev_Utils_Reflection::getRestrictedPropertyValue(
                'Mage',
                '_registry'
            );
        } else {
            EcomDev_Utils_Reflection::setRestrictedPropertyValue(
                'Mage',
                '_registry',
                $registry
            );
        }

        $this->helper = Mage::helper("custoconnector");
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     * @loadFixture population.yaml
     */
    public function testStatusCode()
    {
        Custobar_CustoConnector_Helper_Data::$INITIAL_PAGE_SIZE = 2;

        $this->cleanInitials();

        $this->customerSession(1);

        $this->dispatch(
            "adminhtml/adminhtml_custoconnector_initial_schedule_population/status"
        );

        $this->assertResponseBodyContains(
            'Log in to Admin Panel',
            "Can't get status for non loggedin users"
        );

        $this->adminSession();

        $this->dispatch(
            "adminhtml/adminhtml_custoconnector_initial_schedule_population/status"
        );

        $this->assertResponseBodyContains(
            '{"status":"unknown","message":""}',
            "Can get the unkown status for admin"
        );

        $this->helper->startInitialScheduling();

        $trackedModels = array_keys($this->helper->getConfigTrackedModels());
        $pages = 0;

        foreach ($trackedModels as $trackedModel) {
            if (!$this->helper->getIsModelInternal($trackedModel)) {
                $expectedCollection =
                    Mage::getSingleton($trackedModel)->getCollection();
                /** @var Mage_Core_Model_Abstract $expectedEntity */
                $pages += (int)ceil(
                    $expectedCollection->count()
                    / Custobar_CustoConnector_Helper_Data::$INITIAL_PAGE_SIZE
                );
                foreach ($expectedCollection as $expectedEntity) {
                    $expected[] = [
                        'entity_type' => $trackedModel,
                        'entity_id' => $expectedEntity->getId()
                    ];
                }
            }
        }

        $this->dispatch(
            "adminhtml/adminhtml_custoconnector_initial_schedule_population/status"
        );

        $this->assertResponseBodyContains(
            "{\"status\":\"processing\",\"message\":\"Processed pages 0%\"}",
            "Can get the processing status"
        );

        $this->helper->handleNextInitialPage();

        $this->dispatch(
            "adminhtml/adminhtml_custoconnector_initial_schedule_population/status"
        );

        $percentage = round((1 / $pages) * 100, 2);

        $this->assertResponseBodyContains(
            "{\"status\":\"processing\",\"message\":\"Processed pages {$percentage}%\"}",
            "Can get the processing status"
        );

        Custobar_CustoConnector_Helper_Data::$INITIAL_PAGE_SIZE = 1000;
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     * @loadFixture population.yaml
     * @loadFixture statisticsaction.yaml
     */
    public function testScheduleStatus()
    {
        $this->cleanInitials();

        $this->adminSession();

        $this->dispatch(
            "adminhtml/adminhtml_custoconnector_helper/status"
        );

        $this->assertResponseBodyContains(
            '{"erroneous":2,"unprocessed":3}',
            "Can get the correct amount of status"
        );
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     * @loadFixture population.yaml
     */
    public function testStartAction()
    {
        $this->adminSession();
        // we can call the method two time without an exception
        $this->dispatch(
            "adminhtml/adminhtml_custoconnector_initial_schedule_population/start"
        );

        $this->assertResponseBodyContains(
            "1",
            "Can start"
        );
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     * @loadFixture population.yaml
     */
    public function testStartActionWithParams()
    {
        // restore the count as some other test changes this when running all
        Custobar_CustoConnector_Helper_Data::$INITIAL_PAGE_SIZE = 1000;

        $this->cleanInitials();
        $this->adminSession();

        $this->getRequest()->setMethod('POST')
            ->setPost('model', "Mage_Customer_Model_Customer");

        $this->dispatch(
            "adminhtml/adminhtml_custoconnector_initial_schedule_population/start"
        );

        $this->assertResponseBodyContains(
            "1",
            "Can start"
        );

        /** @var Custobar_CustoConnector_Model_Initial $model */
        $model = Mage::getModel("custoconnector/initial");

        /** @var Custobar_CustoConnector_Model_Mysql4_Initial_Collection $collection */
        $collection = $model->getCollection();

        $collectionData = $collection->getData();
        self::assertCount(1, $collectionData);

        self::assertEquals(1, $collectionData[0]["pages"], "Customer pages");
        self::assertEquals(0, $collectionData[0]["page"]);
        self::assertEquals(
            "Mage_Customer_Model_Customer",
            $collectionData[0]["entity_type"]
        );
    }

    protected function tearDown()
    {
        parent::tearDown();

        $this->cleanInitials();
    }


    private function cleanInitials()
    {
        /** @var Custobar_CustoConnector_Model_Initial $model */
        $model = Mage::getModel("custoconnector/initial");
        $model->cleanInitials();
    }
}
