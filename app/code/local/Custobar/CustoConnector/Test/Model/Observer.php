<?php

/**
 * Class Custobar_CustoConnector_Test_Model_Observer
 *
 * @group Custobar_Observer
 * @group Custobar_Model
 */
class Custobar_CustoConnector_Test_Model_Observer
    extends EcomDev_PHPUnit_Test_Case
{
    /**
     * @var Custobar_CustoConnector_Test_Model_Observer
     */
    protected $_observer;

    private $session = true;

    protected function setUp()
    {
        @session_start();
        parent::setUp();
        $this->_observer = Mage::getSingleton('custoconnector/observer');
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     */
    public function testCustomerEvents()
    {
        $this->clearSchedules();

        /** @var Mage_Customer_Model_Customer $john */
        $john = Mage::getModel("customer/customer")->load(1);
        $this->assertEquals("John Doe", $john->getName());

        // need to fire a change as otherwise the dispatch wont fire
        $john->setData("some_data", "yay");
        $john->save();
        $this->assertEventDispatched("model_save_after");

        /** @var Mage_Customer_Model_Customer $jane */
        $jane = Mage::getModel("customer/customer")->load(2);
        $this->assertEquals("Jane Doe", $jane->getName());

        $jane->setData("some_data", "yay");
        $jane->save();

        /** @var Custobar_CustoConnector_Model_Mysql4_Schedule_Collection $schedules */
        $schedules = Mage::getSingleton("custoconnector/schedule")
            ->getCollection();

        $data = $schedules->getData();

        $this->assertCount(2, $data);

        $this->assertEquals(1, $data[0]["entity_id"]);
        $this->assertEquals(
            "Mage_Customer_Model_Customer",
            $data[0]["entity_type"]
        );

        $this->assertEquals(2, $data[1]["entity_id"]);
        $this->assertEquals(
            "Mage_Customer_Model_Customer",
            $data[1]["entity_type"]
        );

        /** @var Mage_Customer_Model_Customer $whois1 */
        $whois1 = Mage::getModel("customer/customer")
            ->load($data[1]["entity_id"]);
        $this->assertEquals("Jane Doe", $whois1->getName());

        /** @var Mage_Customer_Model_Address $address1 */
        $address1 = Mage::getModel("customer/address")
            ->load($data[0]["entity_id"]);

        $this->assertEquals(
            "Address 123",
            implode(",", $address1->getStreet())
        );

        $this->assertEquals(
            $data[0]["entity_id"],
            $address1->getCustomer()->getId()
        );
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     */
    public function testWebsiteCustomerEvents()
    {
        $this->clearSchedules();

        Mage::app()->getStore()
            ->setConfig(
                "custobar/custobar_custoconnector/allowed_websites",
                ""
            );

        /** @var Mage_Customer_Model_Customer $jane */
        $jane = Mage::getModel("customer/customer")->load(2);
        $this->assertEquals("Jane Doe", $jane->getName());

        $jane->setData("some_data", "yay");
        $jane->save();

        /** @var Custobar_CustoConnector_Model_Mysql4_Schedule_Collection $schedules */
        $schedules = Mage::getSingleton("custoconnector/schedule")
            ->getCollection();
        $data = $schedules->getData();
        $this->assertCount(
            0,
            $data,
            "assert that we have no events as the website was not allowed"
        );
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     */
    public function testAddressEvents()
    {
        $this->clearSchedules();

        /** @var Mage_Customer_Model_Address $johnAddress */
        $johnAddress = Mage::getModel("customer/address")->load(1);
        $this->assertEquals("John Doe", $johnAddress->getName());

        $johnAddress->setStreet("new street from the model");
        $johnAddress->save();

        $this->assertEventDispatched("model_save_after");
        $this->assertEventDispatched("customer_address_save_after");

        /** @var Custobar_CustoConnector_Model_Mysql4_Schedule_Collection $schedules */
        $schedules = Mage::getSingleton("custoconnector/schedule")
            ->getCollection();

        $data = $schedules->getData();

        $this->assertCount(1, $data);

        $this->assertEquals(1, $data[0]["entity_id"]);
        $this->assertEquals(
            "Mage_Customer_Model_Customer",
            $data[0]["entity_type"]
        );

        /** @var Mage_Customer_Model_Address $address1 */
        $address1 = Mage::getModel("customer/address")
            ->load($data[0]["entity_id"]);

        $this->assertEquals(
            "new street from the model",
            implode(",", $address1->getStreet())
        );

        $this->assertEquals(
            $data[0]["entity_id"],
            $address1->getCustomer()->getId()
        );
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     */
    public function testOrderEvents()
    {
        $this->clearSchedules();

        $incrementId = $this->createOrderAndShipIt(Mage::app()->getStore());

        $this->assertNotNull($incrementId, "we got an increment id");

        /** @var Custobar_CustoConnector_Model_Mysql4_Schedule_Collection $schedules */
        $schedules = Mage::getSingleton("custoconnector/schedule")
            ->getCollection();

        $schedules->addFilter("entity_type", "Mage_Sales_Model_Order");

        $this->assertCount(
            1,
            $schedules->getData(),
            "Test we have one schedule for order"
        );
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     */
    public function testOrderBlockedWebsiteEvents()
    {
        $this->markTestIncomplete(
            "Should do a test on preventing order events from websites"
        );
        // this fails on cookie issues. Should figure that out
        // To prevent failing on route exception you should add these to the default fixture
        //     stores/books_eng/web/routers/admin/area: admin
        //     stores/books_eng/web/routers/admin/class: Mage_Core_Controller_Varien_Router_Admin

        $this->clearSchedules();

        Mage::app()->getStore()
            ->setConfig(
                "custobar/custobar_custoconnector/allowed_websites",
                ""
            );


        $this->setCurrentStore("books_eng");
        $this->adminSession();
        $incrementId = $this->createOrderAndShipIt(Mage::app()->getStore());

        $this->assertNotNull($incrementId, "we got an increment id");

        /** @var Custobar_CustoConnector_Model_Mysql4_Schedule_Collection $schedules */
        $schedules = Mage::getSingleton("custoconnector/schedule")
            ->getCollection();

        $schedules->addFilter("entity_type", "Mage_Sales_Model_Order");

        $this->assertCount(
            0,
            $schedules->getData(),
            "Assert that we dont have any schedules as the we dont allow sendign from that website"
        );
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     */
    public function testProductEvents()
    {
        $this->clearSchedules();

        Mage::app()->getStore()
            ->setConfig(
                "custobar/custobar_custoconnector/allowed_websites",
                "2"
            );

        /** @var Mage_Catalog_Model_Product $product */
        $product = Mage::getModel('catalog/product')->load(21001);
        $product->setData("test_data", 1);
        $product->save();

        $storeIds = $product->getStoreIds();

        $product = Mage::getModel('catalog/product')->load(31001);
        $product->setData("test_data", 1);
        $product->save();

        /** @var Custobar_CustoConnector_Model_Mysql4_Schedule_Collection $schedules */
        $schedules = Mage::getSingleton("custoconnector/schedule")
            ->getCollection();

        $schedules->addFilter(
            "entity_type",
            "Custobar_CustoConnector_Test_Model_TestProductModel"
        );

        /** @var Mage_Catalog_Model_Product $product */
        $product =
            Mage::getModel('catalog/product')->setStoreId(21)->load(22111);
        $product->setData("test_data", 1);
        $product->save();

        $this->assertCount(
            count($storeIds) + 1,
            $schedules->getData(),
            "Have we got only 4 schedules present. All translations for the product 21001, but not for the 31001 as its in a website that is not allowed. And single for the 22111."
        );
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     * @loadFixture newsletter
     */
    public function testNewsLetterEvents()
    {
        $this->clearSchedules();

        Mage::app()->getStore()
            ->setConfig(
                "custobar/custobar_custoconnector/allowed_websites",
                "2"
            );

        /** @var Mage_Newsletter_Model_Subscriber $newsletterSubscriber */
        $newsletterSubscriberJojo =
            Mage::getModel("Mage_Newsletter_Model_Subscriber")->load(12);

        $newsletterSubscriberJojo->setStatus(
            Mage_Newsletter_Model_Subscriber::STATUS_UNSUBSCRIBED
        );
        $newsletterSubscriberJojo->save();

        $newsletterSubscriberJohn =
            Mage::getModel("Mage_Newsletter_Model_Subscriber")->load(13);
        $newsletterSubscriberJohn->setStatus(
            Mage_Newsletter_Model_Subscriber::STATUS_SUBSCRIBED
        );
        $newsletterSubscriberJohn->save();

        /** @var Custobar_CustoConnector_Model_Mysql4_Schedule_Collection $schedules */
        $schedules = Mage::getSingleton("custoconnector/schedule")
            ->getCollection();

        $schedules->addFilter(
            "entity_type",
            "Mage_Newsletter_Model_Subscriber"
        );

        $schedulesData = $schedules->getData();
        $this->assertCount(
            1,
            $schedulesData,
            "Assert that we have only 1 newsletter event present"
        );

        self::assertEquals("12", $schedulesData[0]["entity_id"]);
    }


    protected function tearDown()
    {
        @session_destroy();
        parent::tearDown();
        // unregister observer
        Mage::unregister('_singleton/custoconnector/observer');

        $this->clearSchedules();
    }

    /**
     * @param Mage_Core_Model_Store $store
     *
     * @return string
     */

    private function createOrderAndShipIt($store)
    {
        $productIds = array(
            21001,
            22101
        );

        /** @var Mage_Customer_Model_Customer $customer */
        $customer = Mage::getModel('customer/customer')->load(1);

        /** @var Mage_Sales_Model_Quote $quote */
        $quote = Mage::getModel('sales/quote');
        $quote->setStoreId($store->getId());

        $quote->assignCustomer($customer);

        foreach ($productIds as $id) {
            $product = Mage::getModel('catalog/product')->load($id);
            $quote->addProduct($product, new Varien_Object(array('qty' => 1)));
        }

        // Set Sales Order Billing Address
        $quote->getBillingAddress()->addData(
            $customer->getPrimaryBillingAddress()->getData()
        );

        // Set Sales Order Shipping Address
        /** @var Mage_Sales_Model_Quote_Address $shippingAddress */
        $shippingAddress = $quote->getShippingAddress()->addData(
            $customer->getPrimaryBillingAddress()->getData()
        );

        // Collect Rates and Set Shipping & Payment Method
        $shippingAddress->setCollectShippingRates(true)
            ->collectShippingRates()
            ->setShippingMethod('flatrate_flatrate')
            ->setPaymentMethod('checkmo');

        $quote->getPayment()->importData(array('method' => 'checkmo'));

        $quote->collectTotals()->save();

        /** @var Mage_Sales_Model_Service_Quote $service */
        $service = Mage::getModel('sales/service_quote', $quote);
        $service->submitAll();
        $increment_id = $service->getOrder()->getRealOrderId();

        $order = $service->getOrder();
        /** @var Mage_Sales_Model_Order_Invoice $invoice */
        $invoice = $order->prepareInvoice();
        $invoice
            ->setTransactionId($order->getId())
            ->addComment("Invoice created from cron job.")
            ->register();
        $invoice->pay();

        /** @var Mage_Core_Model_Resource_Transaction $transaction_save */
        $transaction_save = Mage::getModel('core/resource_transaction');
        $transaction_save
            ->addObject($invoice)
            ->addObject($invoice->getOrder())
            ->save();
        $shipment = $order->prepareShipment();

        if ($shipment) {
            $shipment->register();
            $order->setIsInProcess(true);

            $transaction_save = Mage::getModel('core/resource_transaction');
            $transaction_save
                ->addObject($shipment)
                ->addObject($shipment->getOrder())
                ->save();
        }
        return $increment_id;
    }

    protected function clearSchedules()
    {
        /** @var Custobar_CustoConnector_Model_Mysql4_Schedule_Collection $schedules */
        $schedules = $this->getAllSchedules();

        /** @var Custobar_CustoConnector_Model_Schedule $schedule */
        foreach ($schedules as $schedule) {
            $schedule->delete();
        }
    }

    /**
     * @return \Custobar_CustoConnector_Model_Mysql4_Schedule_Collection
     */
    private function getAllSchedules()
    {
        /** @var Custobar_CustoConnector_Model_Mysql4_Schedule_Collection $schedules */
        $schedules = Mage::getSingleton("custoconnector/schedule")
            ->getCollection();
        return $schedules;
    }
}
