<?php

/**
 * Class Custobar_CustoConnector_Test_Model_Cron
 *
 * @group Custobar_Cron
 * @group Custobar_Model
 */
class Custobar_CustoConnector_Test_Model_Cron extends EcomDev_PHPUnit_Test_Case
{

    /**
     * @var Custobar_CustoConnector_Helper_Data
     */
    private $helper;

    protected function setUp()
    {
        parent::setUp();
        /** @var Custobar_CustoConnector_Helper_Data $helper */

        $this->helper = Mage::helper("custoconnector");
    }

    /**
     * @test
     * @loadFixture ~Custobar_CustoConnector/default
     */
    public function testCron()
    {
        $this->clearSchedules();
        $this->createEvents();

        /** @var Custobar_CustoConnector_Model_Cron $cron */
        $cron = Mage::getSingleton("custoconnector/cron");

        Mage::app()->getStore()
            ->setConfig("custobar/custobar_custoconnector/mode", 1);

        $cron->callCustobarApi();

        /** @var Custobar_CustoConnector_Model_Schedule $model */
        $model = Mage::getSingleton("custoconnector/schedule");
        $model->cleanSchedules();

        /** @var Custobar_CustoConnector_Model_Mysql4_Schedule_Collection $schedules */
        $schedules = Mage::getSingleton("custoconnector/schedule")
            ->getCollection();

        $this->assertCount(
            0,
            $schedules->getData(),
            "Test that there are no schedules present after sending"
        );
    }


    public function createEvents()
    {
        /** @var Mage_Customer_Model_Customer $john */
        $john = Mage::getModel("customer/customer")->load(1);
        $this->assertEquals("John Doe", $john->getName());

        // need to fire a change as otherwise the dispatch wont fire
        $john->setSomeData("yay");
        $john->save();

        /** @var Mage_Customer_Model_Customer $jane */
        $jane = Mage::getModel("customer/customer")->load(2);
        $this->assertEquals("Jane Doe", $jane->getName());
        $jane->setData("yay", 1);
        $jane->save();

        /** @var Custobar_CustoConnector_Model_Mysql4_Schedule_Collection $schedules */
        $schedules = Mage::getSingleton("custoconnector/schedule")
            ->getCollection();

        $this->assertCount(
            2,
            $schedules->getData(),
            "Test that there are two schedules present"
        );
    }

    protected function tearDown()
    {
        parent::tearDown();

        $this->clearSchedules();
    }

    protected function clearSchedules()
    {
        /** @var Custobar_CustoConnector_Model_Mysql4_Schedule_Collection $schedules */
        $schedules = $this->getAllSchedules();

        /** @var Custobar_CustoConnector_Model_Schedule $schedule */
        foreach ($schedules as $schedule) {
            $schedule->delete();
        }
    }

    /**
     * @return \Custobar_CustoConnector_Model_Mysql4_Schedule_Collection
     */
    private function getAllSchedules()
    {
        /** @var Custobar_CustoConnector_Model_Mysql4_Schedule_Collection $schedules */
        $schedules = Mage::getSingleton("custoconnector/schedule")
            ->getCollection();
        return $schedules;
    }
}
