<?php

/**
 * Class Custobar_CustoConnector_Model_Initial
 *
 * @method int getPages();
 * @method Custobar_CustoConnector_Model_Initial setPages(int $value)
 * @method int getPage();
 * @method Custobar_CustoConnector_Model_Initial setPage(int $value)
 * @method int getEntityType();
 * @method Custobar_CustoConnector_Model_Initial setEntityType(string $value)
 *
 */
class Custobar_CustoConnector_Model_Initial extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init("custoconnector/initial");
    }

    /**
     * @return Custobar_CustoConnector_Model_Initial
     */
    public function cleanInitials()
    {
        /** @var Custobar_CustoConnector_Model_Mysql4_Initial $res */
        $res = $this->_getResource();
        $res->removeAll();
        return $this;
    }
}
