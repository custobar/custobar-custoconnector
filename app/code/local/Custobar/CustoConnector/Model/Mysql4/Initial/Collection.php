<?php

class Custobar_CustoConnector_Model_Mysql4_Initial_Collection
    extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init("custoconnector/initial");
    }
}
