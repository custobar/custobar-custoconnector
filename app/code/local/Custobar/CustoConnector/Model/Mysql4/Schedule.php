<?php

class Custobar_CustoConnector_Model_Mysql4_Schedule
    extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init("custoconnector/schedule", "id");
    }

    /**
     * @return Custobar_CustoConnector_Model_Mysql4_Schedule
     */
    public function removeProcessedSchedules()
    {
        $this->_getWriteAdapter()
            ->delete(
                $this->getMainTable(),
                'processed_at != "0000-00-00 00:00:00"'
            );
        return $this;
    }
}
