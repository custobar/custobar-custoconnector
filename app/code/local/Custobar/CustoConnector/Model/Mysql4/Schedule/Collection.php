<?php

class Custobar_CustoConnector_Model_Mysql4_Schedule_Collection
    extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init("custoconnector/schedule");
    }

    /**
     * Add filter by only ready for sending item
     *
     * @return Custobar_CustoConnector_Model_Mysql4_Schedule_Collection
     */
    public function addOnlyForSendingFilter()
    {
        $this->getSelect()
            ->where('main_table.processed_at = "0000-00-00 00:00:00"');
        return $this;
    }

    public function addOnlyErroneousFilter()
    {
        $this->addOnlyForSendingFilter();
        $this->getSelect()
            ->where('main_table.errors > 0');
        return $this;
    }
}
