<?php

class Custobar_CustoConnector_Model_Mysql4_Initial
    extends Mage_Core_Model_Mysql4_Abstract
{
    public function removeAll()
    {
        $this->_getWriteAdapter()
            ->delete(
                $this->getMainTable()
            );
        return $this;
    }

    protected function _construct()
    {
        $this->_init("custoconnector/initial", "id");
    }
}
