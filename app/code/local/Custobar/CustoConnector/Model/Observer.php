<?php

class Custobar_CustoConnector_Model_Observer
{
    private $_helper;

    /**
     * @param Varien_Event_Observer $observer
     */
    public function modelSaveAfter($observer)
    {
        /** @var Custobar_CustoConnector_Helper_Data $ch */
        $ch = $this->getCHelper();
        try {
            if ($ch->canProcess()) {
                /** @var Mage_Core_Model_Abstract $ob */
                $ob = $observer->getObject();
                $modelClassName = get_class($ob);
                $trackedModelClassName = $ch->getEvenObjectTrackedModel($ob);

                if ($trackedModelClassName) {
                    if ($ch->getIsModelInternal($trackedModelClassName)) {
                        $mapped = $ch->getMappedEntity($ob);
                        $mapSet = $ch->getModelMapSet($trackedModelClassName);
                        $toClass = $mapSet["to"];

                        if (
                            !$mapped->isEmpty()
                            && $mapSet
                            && isset($toClass)
                            && $mapped->getId()
                        ) {
                            $targetMapSet =
                                $ch->getModelMapSet($toClass);

                            if ($targetMapSet) {
                                $entityFromTo = $ch->getEntityByTypeAndId(
                                    $toClass,
                                    $mapped->getId(),
                                    $ob->getStoreId()
                                );

                                if ($ch->getCanTrackObject(
                                    $entityFromTo
                                )
                                ) {
                                    $this->scheduleByStoreIdsIfPresent(
                                        $entityFromTo,
                                        $toClass
                                    );
                                } else {
                                    $ch->log(
                                        "Rejected tracking item {$mapped->getId()} of {$toClass}"
                                    );
                                }
                            }
                        }
                    } else {
                        if ($ch->getCanTrackObject($ob)) {
                            $this->scheduleByStoreIdsIfPresent(
                                $ob,
                                $modelClassName
                            );
                        } else {
                            $ch->log(
                                "Rejected tracking item {$ob->getId()} of {$modelClassName}"
                            );
                        }
                    }
                }
            }
        } catch (Exception $e) {
            $message = $e->getMessage();
            $ch->log("Model after save failed {$message}");
        }
    }

    /**
     * @param Varien_Event_Observer $observer
     */
    public function extendCustomerData($observer)
    {
        /** @var Mage_Customer_Model_Customer $customer */
        $customer = $observer->getEntity();

        /** @var Mage_Sales_Model_Order_Address $defaultAddress */
        $defaultAddress = $customer->getPrimaryBillingAddress();

        $additionalData = [];

        if ($defaultAddress) {
            $additionalData["custobar_street"] =
                $defaultAddress->getStreetFull();
            $additionalData["custobar_city"] = $defaultAddress->getCity();
            $additionalData["custobar_country_id"] =
                $defaultAddress->getCountryId();
            $additionalData["custobar_region"] =
                $defaultAddress->getRegion();
            $additionalData["custobar_postcode"] =
                $defaultAddress->getPostcode();
            $additionalData["custobar_telephone"] =
                $defaultAddress->getTelephone();
        }

        $additionalData["custobar_created_at"] =
            date(Zend_Date::ISO_8601, $customer->getCreatedAtTimestamp());


        $customer->addData($additionalData);
    }

    /**
     * @param Varien_Event_Observer $observer
     */
    public function extendProductData($observer)
    {
        /** @var Mage_Catalog_Model_Product $product */
        $product = $observer->getEntity();
        /** @var array $map */
        $map = $observer->getMap();

        $attributes = $product->getAttributes();

        $additionalData = [];

        /** @var \Mage_Eav_Model_Entity_Attribute_Set $attributeSetModel */
        $attributeSetModel = Mage::getModel("eav/entity_attribute_set");
        $attributeSetModel->load($product->getAttributeSetId());
        $attributeSetName = $attributeSetModel->getAttributeSetName();

        $additionalData["custobar_price"] =
            round((float)$product->getPrice() * 100);

        $additionalData["custobar_minimal_price"] =
            round((float)$product->getMinimalPrice() * 100);

        $additionalData["custobar_attribute_set_name"] = $attributeSetName;

        /** @var array $storeIds */
        $storeIds = $product->getStoreIds();
        $storeId = 0;


        if ($product->getStore()) {
            $storeId = $product->getStore()->getId();
        } else {
            // Lets get the default store and check if product is in it > if so use that to get the ur
            $defaultStoreId = Mage::app()->getDefaultStoreView()->getId();
            if (in_array($defaultStoreId, $storeIds)) {
                $storeId = $defaultStoreId;
            } else {
                $storeId = array_shift($storeIds);
            }
        }

        if ($product->isVisibleInSiteVisibility()) {
            $url =
                $this->rewrittenProductUrl($product->getId(), null, $storeId);

            if ($url) {
                $url = Mage::app()->getStore($storeId)->getUrl($url);
            } else {
                $product->getUrlInStore(
                    ["_store" => $storeId]
                );
            }
            $additionalData["custobar_product_url"] = $url;
        }

        $categories =
            $product->getCategoryCollection()
                ->setStoreId($storeId)
                ->addAttributeToSelect(
                    'name'
                );

        $categoriesNames = [];
        $categoriesIds = [];

        foreach ($categories as $category) {
            $categoriesNames[] = $category->getName();
            $categoriesIds[] = $category->getId();
        }

        //$product->getUrlInStore($thi->getDefaultStore); < jokseenkin noin

        $additionalData["custobar_category"] = implode($categoriesNames, ",");
        $additionalData["custobar_category_id"] = implode($categoriesIds, ",");

        $additionalData["custobar_special_price"] =
            round((float)$product->getSpecialPrice() * 100);

        $locale = Mage::getStoreConfig(
            Mage_Core_Model_Locale::XML_PATH_DEFAULT_LOCALE,
            $storeId
        );

        $lang = "fi";

        if ($locale) {
            $localeArray = explode('_', $locale);
            if (isset($localeArray[0])) {
                $lang = $localeArray[0];
            }
        }

        $additionalData["custobar_language"] = $lang;
        $additionalData["custobar_store_id"] = $product->getStoreId();

        // product raw image
        $additionalData["custobar_image"] =
            Mage::getModel('catalog/product_media_config')
                ->getMediaUrl($product->getImage());

        $typeInstance = $product->getTypeInstance();

        if ($product->getTypeId() == "configurable") {
            $additionalData = $this->getConfigurableChildren(
                $typeInstance,
                $product,
                $additionalData
            );
        }

        if ($product->getTypeId() == "bundle") {
            $additionalData = $this->getBundleChildren(
                $typeInstance,
                $product,
                $storeId,
                $additionalData
            );
        }

        if ($product->getTypeId() == "grouped") {
            $additionalData = $this->getGroupedChildren(
                $typeInstance,
                $product,
                $storeId,
                $additionalData
            );
        }

        /** @var \Mage_Catalog_Model_Product_Type_Configurable $configurableSingleton */
        $configurableSingleton =
            Mage::getSingleton("catalog/product_type_configurable");

        $configurableParents = $configurableSingleton->getParentIdsByChild(
            $product->getId()
        );

        /** @var \Mage_Bundle_Model_Product_Type $bundleSingleton */
        $bundleSingleton =
            Mage::getSingleton("bundle/product_type");

        $bundleParents =
            $bundleSingleton->getParentIdsByChild($product->getId());

        /** @var \Mage_Catalog_Model_Product_Type_Grouped $groupedSingleton */
        $groupedSingleton =
            Mage::getSingleton("catalog/product_type_grouped");

        $groupedParents = $groupedSingleton->getParentIdsByChild(
            $product->getId()
        );

        $parent_ids = array_unique(
            array_merge(
                $configurableParents,
                $bundleParents,
                $groupedParents
            )
        );

        // bundle item appears as a parent also for some reason. Pop at least that out
        if (($key = array_search($product->getId(), $parent_ids)) !== false) {
            unset($parent_ids[$key]);
        }

        if (count($parent_ids) > 0) {
            $parentSkus = $this->getProductSkusByID($storeId, $parent_ids);

            if (count($parentSkus) > 0) {
                $additionalData["custobar_parent_ids"] =
                    implode(",", $parentSkus);
            }
        }

        foreach ($map as $index => $item) {
            if (strpos($index, "custobar") === false
                && key_exists($index, $attributes)) {

                /** @var \Mage_Catalog_Model_Entity_Attribute $productAttribute */
                $productAttribute = $attributes[$index];

                $input = $productAttribute->getFrontend()->getConfigField(
                    'input'
                );

                // skip if not a dropdown element
                if (!in_array($input, ['select', 'multiselect'])) {
                    continue;
                }

                $productResource = $product->getResource();

                $attributeRawValue =
                    $productResource->getAttributeRawValue(
                        $product->getId(),
                        $index,
                        $storeId
                    );

                if ($attributeRawValue) {
                    $product->setData($index, $attributeRawValue);
                    $additionalData[$index] =
                        $productAttribute->getFrontend()->getValue(
                            $product
                        );
                }
            }
        }

        $product->addData($additionalData);
    }

    /**
     * @param Varien_Event_Observer $observer
     */
    public function extendOrderData($observer)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getEntity();

        /** @var array $additionalData */
        $additionalData = [];

        /** @var Zend_Date $createdAtZend */
        $createdAtZend = $order->getCreatedAtDate();

        $additionalData["custobar_created_at"] =
            $createdAtZend->toString(Zend_Date::ISO_8601);

        $additionalData["custobar_discount"] =
            round((float)$order->getDiscountAmount() * 100);

        $additionalData["custobar_grand_total"] =
            round((float)$order->getGrandTotal() * 100);

        $payment = $order->getPayment();
        if ($payment) {
            $additionalData["custobar_payment_method"] =
                $payment->getMethod();
        }

        $visibleItems = $order->getAllVisibleItems();

        $additionalData["custobar_order_items"] = [];

        /** @var Mage_Sales_Model_Order_Item $item */
        foreach ($visibleItems as $item) {
            $orderItem = [];
            $orderItem["sale_external_id"] = $order->getIncrementId();
            $orderItem["external_id"] = $item->getId();
            $orderItem["product_id"] = $item->getSku();
            $orderItem["unit_price"] =
                round((float)$item->getPriceInclTax() * 100);
            $orderItem["quantity"] = $item->getQtyOrdered();
            $orderItem["discount"] =
                round((float)$item->getDiscountAmount() * 100);
            $orderItem["total"] =
                round(
                    (float)$item->getRowTotalInclTax() * 100
                    - (float)$item->getDiscountAmount() * 100
                );
            $additionalData["custobar_order_items"][] = $orderItem;
        }

        $additionalData["custobar_state"] = strtoupper($order->getState());
        $order->addData($additionalData);
    }

    /**
     * @param Varien_Event_Observer $observer
     */
    public function extendNewsletterSubscriberData($observer)
    {
        /** @var Mage_Newsletter_Model_Subscriber $newsletterSubscriber */
        $newsletterSubscriber = $observer->getEntity();

        /** @var array $additionalData */
        $additionalData = [];
        //custobar_status>type;
        //custobar_change_status_at>date;

        $status = "MAIL_UNSUBSCRIBE";

        if ($newsletterSubscriber->getStatus()
            == Mage_Newsletter_Model_Subscriber::STATUS_SUBSCRIBED
        ) {
            $status = "MAIL_SUBSCRIBE";
        }

        $additionalData["custobar_date"] =
            $this->getCreatedAtDateAsIsoISO8601(Varien_Date::formatDate(true));
        $additionalData["custobar_status"] = $status;

        $newsletterSubscriber->addData($additionalData);
    }

    /**
     * @param Varien_Event_Observer $observer
     */
    public function extendStoreData($observer)
    {
        /** @var Mage_Core_Model_Store $store */
        $store = $observer->getEntity();
        $store->setData(
            "custobar_name",
            "{$store->getWebsite()->getName()}, {$store->getGroup()->getName()}, {$store->getName()}"
        );
    }


    /**
     * Get object created at date affected current active store timezone
     *
     * @param string $createdAt
     *
     * @return \Zend_Date
     */
    public function getCreatedAtDateAsIsoISO8601($createdAt)
    {
        return Mage::app()->getLocale()->date(
            Varien_Date::toTimestamp($createdAt),
            null,
            null,
            true
        )->toString(Zend_Date::ISO_8601);
    }

    public function rewrittenProductUrl($productId, $categoryId, $storeId)
    {
        /** @var Mage_Core_Model_Url_Rewrite $coreUrl */
        $coreUrl = Mage::getModel('core/url_rewrite');
        $idPath = sprintf('product/%d', $productId);
        if ($categoryId) {
            $idPath = sprintf('%s/%d', $idPath, $categoryId);
        }
        $coreUrl->setStoreId($storeId);
        $coreUrl->loadByIdPath($idPath);

        return $coreUrl->getRequestPath();
    }

    /**
     * @param Mage_Core_Model_Abstract $ob
     * @param string                   $modelClassName
     */
    public function scheduleByStoreIdsIfPresent($ob, $modelClassName)
    {
        $storeIds = $ob->getStoreIds();

        if (!$ob->getStoreId() && $storeIds && is_array($storeIds)
            && count(
                $storeIds
            ) > 0
        ) {
            /** @var int $storeId */
            foreach ($storeIds as $storeId) {
                $this->getCHelper()->scheduleUpdate(
                    $ob->getId(),
                    $storeId,
                    $modelClassName
                );
            }
        } else {
            $this->getCHelper()->scheduleUpdate(
                $ob->getId(),
                $ob->getStoreId(),
                $modelClassName
            );
        }
    }

    /**
     * @return \Custobar_CustoConnector_Helper_Data
     */
    public function getCHelper()
    {
        if (!$this->_helper) {
            $this->_helper = Mage::helper("custoconnector");
        }

        return $this->_helper;
    }

    private function getRawMediaUrl($product, $code)
    {
        return (string)$this->_getImageHelper()
            ->init($product, $code);

    }

    /**
     * Return Catalog Product Image helper instance
     *
     * @return Mage_Catalog_Helper_Image
     */
    protected function _getImageHelper()
    {
        return Mage::helper('catalog/image');
    }

    /**
     * @param $typeInstance \Mage_Catalog_Model_Product_Type_Configurable
     * @param $product
     * @param $additionalData
     *
     * @return mixed
     */
    private function getConfigurableChildren(
        $typeInstance,
        $product,
        $additionalData
    ) {
        $childProducts = $typeInstance->getUsedProducts($product);

        if ($childProducts) {
            $childSkus = [];
            foreach ($childProducts as $childProduct) {
                $childSkus[] = $childProduct->getSku();
            }

            $additionalData["custobar_child_ids"] =
                implode(",", $childSkus);
        }
        return $additionalData;
    }

    /**
     * @param $storeId
     * @param $parent_ids
     *
     * @return array
     */
    protected function getProductSkusByID($storeId, $parent_ids)
    {
        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection = Mage::getModel("catalog/product")
            ->setStoreId($storeId)
            ->getCollection();

        $collection->setStoreId($storeId);
        $collection
            ->addAttributeToSelect(["sku"])
            ->addAttributeToFilter("entity_id", ["in" => $parent_ids]);

        return $collection->load()->getColumnValues("sku");
    }


    /**
     * @param $typeInstance \Mage_Bundle_Model_Product_Type
     * @param $product
     * @param $storeId
     * @param $additionalData
     *
     * @return mixed
     */
    private function getBundleChildren(
        $typeInstance,
        $product,
        $storeId,
        $additionalData
    ) {
        $childIds =
            $typeInstance->getChildrenIds(
                $product->getId(),
                false
            );


        if (count($childIds) > 0) {
            $additionalData["custobar_child_ids"] =
                implode(
                    ",",
                    $this->getProductSkusByID($storeId, $childIds)
                );
        }
        return $additionalData;
    }

    /**
     * @param $typeInstance \Mage_Catalog_Model_Product_Type_Grouped
     * @param $product
     * @param $storeId
     * @param $additionalData
     *
     * @return mixed
     */
    private function getGroupedChildren(
        $typeInstance,
        $product,
        $storeId,
        $additionalData
    ) {

        $childProducts = $typeInstance->getAssociatedProducts($product);

        if ($childProducts) {
            $childSkus = [];
            foreach ($childProducts as $childProduct) {
                $childSkus[] = $childProduct->getSku();
            }

            $additionalData["custobar_child_ids"] =
                implode(",", $childSkus);
        }
        return $additionalData;
    }

    /**
     * @param Varien_Event_Observer $observer
     */
    public function modelDeleteAfter($observer)
    {
        /** @var Custobar_CustoConnector_Helper_Data $ch */
        $ch = $this->getCHelper();
        try {

            /** @var Mage_Core_Model_Abstract $ob */
            $ob = $observer->getObject();
            $modelClassName = get_class($ob);

            $this->getCHelper()->unScheduleUpdate(
                $ob->getId(),
                $modelClassName
            );

        } catch (Exception $e) {
            $message = $e->getMessage();
            $ch->log("Model after delete failed {$message}");
        }
    }
}
