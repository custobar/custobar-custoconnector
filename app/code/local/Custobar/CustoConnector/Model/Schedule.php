<?php

/**
 * Class Custobar_CustoConnector_Model_Schedule
 *
 * @method int getStoreId();
 * @method Custobar_CustoConnector_Model_Schedule setStoreId(int $value)
 * @method int getEntityId();
 * @method Custobar_CustoConnector_Model_Schedule setEntityId(int $value)
 * @method int getEntityType();
 * @method Custobar_CustoConnector_Model_Schedule setEntityType(string $value)
 * @method Custobar_CustoConnector_Model_Schedule setCreatedAt(string $value)
 * @method string getProcessedAt()
 * @method Custobar_CustoConnector_Model_Schedule setProcessedAt(string $value)
 * @method Custobar_CustoConnector_Model_Schedule setErrors(int $value)
 *
 */
class Custobar_CustoConnector_Model_Schedule extends Mage_Core_Model_Abstract
{
    public function getErrorsCount()
    {
        return (int)$this->getErrors();
    }

    protected function _construct()
    {
        $this->_init("custoconnector/schedule");
    }

    /**
     * @return Custobar_CustoConnector_Model_Schedule
     */
    public function cleanSchedules()
    {
        /** @var Custobar_CustoConnector_Model_Mysql4_Schedule $res */
        $res = $this->_getResource();
        $res->removeProcessedSchedules();
        return $this;
    }

    public function initialPopulation()
    {
        /** @var Custobar_CustoConnector_Helper_Data $helper */
        $helper = Mage::helper('custoconnector');
        $helper->handleNextInitialPage();
    }
}
