<?php

class Custobar_CustoConnector_Model_Cron
{
    public function callCustobarApi()
    {
        /** @var Custobar_CustoConnector_Helper_Data $helper */
        $helper = Mage::helper("custoconnector");

        if (!$helper->isInitialRunning()) {
            $helper->sendUpdateRequests();
        } else {
            $helper->log(
                "Initial scheduling running. Will not purge schedules"
            );
        }
    }
}
