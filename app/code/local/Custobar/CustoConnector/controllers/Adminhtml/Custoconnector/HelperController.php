<?php

class Custobar_CustoConnector_Adminhtml_Custoconnector_HelperController
    extends Mage_Adminhtml_Controller_Action
{
    public function statusAction()
    {
        /** @var Mage_Core_Controller_Response_Http $response */
        $response = $this->getResponse();
        $response->setHeader('Content-Type', 'application/json');

        $erroneousCount = $this->getErroneousCount();
        $unprocessedCount = $this->getUnProcessedCount();

        $response->setBody(
            json_encode(
                [
                    'erroneous' => $erroneousCount,
                    'unprocessed' => $unprocessedCount
                ]
            )
        );
        return true;
    }

    /**
     * @return int
     */
    private function getErroneousCount()
    {
        $collection = $this->getCHelper()->getSchedulesCollection();
        $collection->addOnlyErroneousFilter();
        return $collection->getSize();
    }

    /**
     * @return int
     */
    private function getUnProcessedCount()
    {
        $collection = $this->getCHelper()->getSchedulesCollection();
        $collection->addOnlyForSendingFilter();
        return $collection->getSize();
    }

    /**
     * @return Custobar_CustoConnector_Helper_Data
     */
    protected function getCHelper()
    {
        return Mage::helper("custoconnector");
    }
}
