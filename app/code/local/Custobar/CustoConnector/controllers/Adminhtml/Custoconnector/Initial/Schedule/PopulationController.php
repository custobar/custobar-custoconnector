<?php

class Custobar_CustoConnector_Adminhtml_Custoconnector_Initial_Schedule_PopulationController
    extends Mage_Adminhtml_Controller_Action
{
    public function startAction()
    {
        /** @var Mage_Core_Controller_Response_Http $response */
        $response = $this->getResponse();
        $response->setHeader('Content-Type', 'application/json');

        if ($this->isProcessing()
        ) {
            $response->setHeader('Status', '403');
            $response->setBody(
                json_encode(
                    [
                        'error' => [
                            'message' => 'The job exists already'
                        ]
                    ]
                )
            );
            return false;
        }
        try {
            $model = $this->getRequest()->getParam("model", null);
            $this->getCHelper()->startInitialScheduling($model);
            $response->setBody(true);
            return true;
        } catch (Exception $e) {
            $response->setHeader('Status', '500');
            $response->setBody(
                json_encode(
                    [
                        'error' => [
                            'code' => $e->getCode(),
                            'message' => $e->getMessage()
                        ]
                    ]
                )
            );
            return false;
        }
    }

    public function statusAction()
    {
        /** @var Mage_Core_Controller_Response_Http $response */
        $response = $this->getResponse();
        $response->setHeader('Content-Type', 'application/json');
        $status = 'unknown';
        $message = '';

        if ($this->isProcessing()) {
            $status = 'processing';
            $message = $this->isProcessing();
        }
        $response->setBody(
            json_encode(
                [
                    'status' => $status,
                    'message' => $message
                ]
            )
        );
        return true;
    }

    /**
     * @return bool|string
     */
    protected function isProcessing()
    {
        $collection = $this->getInitialCollection();

        if ($collection->count() > 0) {
            $pages = 0;
            $processedPages = 0;

            /** @var Custobar_CustoConnector_Model_Initial $item */
            foreach ($collection as $item) {
                $pages += (int)$item->getPages();
                $processedPages += (int)$item->getPage();
            }

            $percentage = 0;
            if ($processedPages > 0) {
                $percentage = round(($processedPages / $pages) * 100, 2);
            }

            return "Processed pages {$percentage}%";
        }
        return false;
    }

    /**
     * @return Custobar_CustoConnector_Helper_Data
     */
    protected function getCHelper()
    {
        return Mage::helper("custoconnector");
    }

    /**
     * @return Custobar_CustoConnector_Model_Mysql4_Initial_Collection
     */
    protected function getInitialCollection()
    {
        return Mage::getModel("custoconnector/initial")->getCollection();
    }
}
